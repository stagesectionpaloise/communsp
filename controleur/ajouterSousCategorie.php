<?php
//on verifie si il y a une session et si il y en a pas on demarre la session
if (!isset($_SESSION))
{
    session_start();
}

//appel du fichier pour empecher a des personnes non identifie
//d'acceder a cette page
include_once "protectionPage.php";
//appel du fichier pour empecher une personne identifié non staff d'acceder a la page
include_once "protectionStaff.php";

//appel des fichier qui contienne les fonctions pour les utiliser plus tard
include_once "../modele/bd.admin.inc.php";

//recuperation des donnees du formulaire
$libSousCategorie = $_POST['libSousCategorie'];
$idCategorie = $_POST['idCategorie'];

//verification du token et verification de sa duree
//il s'efface au bout d'une heure et deconnecte la personne
if(isset($_SESSION['token']) && isset($_SESSION['token_time']) && isset($_POST['token']))
{
	if($_SESSION['token'] == $_POST['token'])
	{
		$timestamp_ancien = time() - (60*60);
		if($_SESSION['token_time'] >= $timestamp_ancien)
		{
      //fonction qui ajoute un item
      addSousCategorie($libSousCategorie, $idCategorie);
      //redirection vers la page admin
      header('Location: pageAdmin.php');
    }
    else
    {
      //fonction qui enleve la session en cours
      session_unset();
      //redirection vers la page de connexion
      header('Location: connexion.php');
    }
  }
  else
  {
    session_unset();
    header('Location: connexion.php');
  }
}
else
{
  session_unset();
  header('Location: connexion.php');
}

?>
