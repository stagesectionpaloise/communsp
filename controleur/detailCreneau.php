<?php
//on verifie si il y a une session et si il y en a pas on demarre la session
if (!isset($_SESSION))
{
    session_start();
}

//appel de la page modéle pour utiliser les fonctions
include_once "../modele/creSeance.inc.php";

if (isset($_POST["Ladate"]))
{
  //recuperation des donnees du formulaire
  $DateEntr = $_POST["Ladate"];
  $heureDebut = $_POST["heureDebut"];
  $heureFin = $_POST["heureFin"];
  $idSeance = $_POST["idSeance"];
  $idCreneau = $_POST["idCreneau"];
  $tabLibelCateg = $_POST["tabLibelCateg"];
  $tabLibelSousCateg = $_POST["tabLibelSousCateg"];
  $libelItem = $_POST["libelItem"];
  $duree = $_POST["duree"];

  //recuperation des donnees entraineur, joueur selon la presence
  $entraineur = getEntraineurByStatus();
  $joueurs = getMembrebystatus();
  $idRetenu = array();
  $statut = 2;
  $joueursPres = getMembresbyPresent($idCreneau, $statut);
  $statut = 1;
  $entraineursPresent = getMembresbyPresent($idCreneau, $statut);
}

//appel du fichier pour empecher a des personnes non identifie
//d'acceder a cette page
include_once "protectionPage.php";
//appel du fichier pour empecher une personne identifié non staff d'acceder a la page
include_once "protectionStaff.php";

//appel des pages pour l'affichage
include "../vue/entete.html.php";
include "../vue/vueDetailCreneau.php";
include "../vue/pied.html.php";
