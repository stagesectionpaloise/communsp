<?php
  //permet de rediriger un joueur vers la page d'accueil
  //si une personne identifié autre qu'un membre du staff essaie d'entrer sur la page

  if($_SESSION["statut"] == 2)
  {
    header('Location: visualisationJoueur.php');
  }

?>
