<?php
//on verifie si il y a une session et si il y en a pas on demarre la session
if (!isset($_SESSION))
{
    session_start();
}
include_once "protectionPage.php";
include_once "../modele/bd.joueur.inc.php";

if(isset($_SESSION['token']) && isset($_SESSION['token_time']) && isset($_POST['token']))
{
	if($_SESSION['token'] == $_POST['token'])
	{
		$timestamp_ancien = time() - (60*60);
		if($_SESSION['token_time'] >= $timestamp_ancien)
		{
      //récupére les données du compte si ils ne sont pas changés
      $idMembre = $_POST["idMembre"];
      $prenom = $_POST["prenom"];
      $nom = $_POST["nom"];
      $username = $_POST["username"];
      $mail = $_POST["mail"];
      $tel = $_POST["tel"];
      $dateNaissance = date('Y-m-d');
      $dateNaissance = $_POST["dateNaissance"];

      updateInfoCompte($prenom, $nom ,$username, $mail, $tel, $dateNaissance, $idMembre);

      header('Location: monCompte.php');
		}
    else
    {
      session_unset();
      header('Location: connexion.php');
    }
  }
  else
  {
    session_unset();
    header('Location: connexion.php');
  }
}
else
{
  session_unset();
  header('Location: connexion.php');
}

?>
