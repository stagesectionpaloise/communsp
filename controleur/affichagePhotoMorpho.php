<?php
//on verifie si il y a une session et si il y en a pas on demarre la session
if (!isset($_SESSION))
{
    session_start();
}
//appel du fichier pour empecher a des personnes non identifie
//d'acceder a cette page
include_once "protectionPage.php";

//appel des fichier qui contienne les fonctions pour les utiliser plus tard
include_once "../modele/bd.joueur.inc.php";
include_once "../modele/bd.poids.inc.php";

//recuperation des donnes du formulaire afin de les traiter
$idMembre = $_POST['idMembre'];
$uneDate = $_POST['dateMorpho'];
$unLienPhoto = $_POST['lienPhoto'];

//recuperation des donnees d'un joueur grace a une fonction
$unJoueur = getJoueurByIdMembre($idMembre);

//si le bouton a ete clique alors la modification se produit
if (isset($_POST["modif"]))
{
  if ($_POST["modif"]==1)
  {
    //verification du token et verification de sa duree
    //il s'efface au bout d'une heure et deconnecte la personne
    if(isset($_SESSION['token']) && isset($_SESSION['token_time']) && isset($_POST['token']))
    {
    	if($_SESSION['token'] == $_POST['token'])
    	{
    		$timestamp_ancien = time() - (60*60);
    		if($_SESSION['token_time'] >= $timestamp_ancien)
    		{
          //recuperation des donnees du formulaire
          $name=NULL;
          $poids = $_POST["poids"];
          $taille = $_POST["taille"];
          $masseGraisseuse = $_POST["masseGraisseuse"];

          //recuperation de la photo
          if(isset($_FILES['file']))
          {
              $tmpName = $_FILES['file']['tmp_name'];
              $name = $_FILES['file']['name'];
              move_uploaded_file($tmpName, '../photoJoueurPoids/'.$name);
          }

          //appel de la fonction de modification pour le poids
          updatePoids($uneDate, $idMembre, $poids, $taille, $masseGraisseuse, $name);
        }
        else
        {
          //fonction qui enleve la session en cours
          session_unset();
          //redirection vers la page de connexion
          header('Location: connexion.php');
        }
  		}
      else
      {
        session_unset();
        header('Location: connexion.php');
      }
  	}
    else
    {
      session_unset();
      header('Location: connexion.php');
    }
  }
}

//appel des pages pour l'affichage
include "../vue/entete.html.php";
include "../vue/vueAffichagePhotoMorpho.php";
include "../vue/pied.html.php";
?>
