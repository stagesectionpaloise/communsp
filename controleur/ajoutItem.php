<?php
//on verifie si il y a une session et si il y en a pas on demarre la session
if (!isset($_SESSION))
{
    session_start();
}

//appel du fichier pour empecher a des personnes non identifie
//d'acceder a cette page
include_once "protectionPage.php";
//appel du fichier pour empecher une personne identifié non staff d'acceder a la page
include_once "protectionStaff.php";

//appel des fichier qui contienne les fonctions pour les utiliser plus tard
include_once "../modele/bd.joueur.inc.php";

//titre pour l'onglet
$titre="Ajout Item";

//fonction qui recupere toutes les categories
$categorie = getCategorie();

//si la page a recu la donnee 'categorie' du formulaire alors on recupere les sous categorie en fonction de la categorie
if(isset($_POST['categorie']))
{
  $souscategorie = getSousCategorieByIdCategorie($_POST['categorie']);
}

//appel des pages pour l'affichage
include "../vue/entete.html.php";
include "../vue/vueAjoutItem.php";
include "../vue/pied.html.php";
 ?>
