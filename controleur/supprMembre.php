<?php
//permet de rediriger un joueur vers la page d'accueil
if (!isset($_SESSION))
{
    session_start();
}
include_once "protectionPage.php";
include_once "protectionStaff.php";
include_once "../modele/bd.joueur.inc.php";
$titre="Supprimer membre";

$listeJoueur = getListeMembreByStatut(2);
$listeStaff = getListeMembreByStatut(1);

//appel des pages pour l'affichage
include "../vue/entete.html.php";
include "../vue/vueSupprMembre.php";
include "../vue/pied.html.php";
 ?>
