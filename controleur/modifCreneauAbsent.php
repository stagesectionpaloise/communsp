<?php
//on verifie si il y a une session et si il y en a pas on demarre la session
if (!isset($_SESSION))
{
    session_start();
}

//appel de la page modéle pour utiliser les fonctions
include_once "../modele/creSeance.inc.php";

//recuperation des donnees du formulaire
$idCreneau = $_POST["idCreneau"];
$idSeance = $_POST['idSeance'];

//recuperation des joueurs et entraineurs absents
$entraineursAbsent = getMembresbyAbsent($idCreneau, 1);
$joueursAbsent = getMembresbyAbsent($idCreneau, 2);

//appel du fichier pour empecher a des personnes non identifie
//d'acceder a cette page
include_once "protectionPage.php";
//appel du fichier pour empecher une personne identifié non staff d'acceder a la page
include_once "protectionStaff.php";

//appel des pages pour l'affichage
include "../vue/entete.html.php";
include "../vue/vueModifCreneauAbsent.php";
include "../vue/pied.html.php";


//
for ($i = 0; $i < count($joueursAbsent); $i++)
{
  if (isset($_POST["joueur"]))
  {
    $joueursCoche = $_POST["joueur"];
    if (in_array($joueursAbsent[$i]['idMembre'], $joueursCoche)==true)
    {
      modificationPresent($joueursAbsent[$i]['idMembre'], $idCreneau, 1);
    }
    else
    {
      modificationPresent($joueursAbsent[$i]['idMembre'], $idCreneau, 0);
    }
  }
  else
  {
    modificationPresent($joueursAbsent[$i]['idMembre'], $idCreneau, 0);
  }
}
for ($i = 0;$i < count($entraineursAbsent); $i++)
{
  if (isset($_POST["entraineur"]))
  {
    $entraineursCoche = $_POST["entraineur"];
    if (in_array($entraineursAbsent[$i]['idMembre'], $entraineursCoche)==true)
    {
      modificationPresent($entraineursAbsent[$i]['idMembre'], $idCreneau, 1);
    }
    else
    {
      modificationPresent($entraineursAbsent[$i]['idMembre'], $idCreneau, 0);
    }
  }
  else
  {
    modificationPresent($entraineursAbsent[$i]['idMembre'], $idCreneau, 0);
  }
}
