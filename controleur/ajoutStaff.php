<?php
//on verifie si il y a une session et si il y en a pas on demarre la session
if (!isset($_SESSION))
{
    session_start();
}

//appel de la page modéle pour utiliser les fonctions
include_once "../modele/creSeance.inc.php";

//affichage de l'entete
include "../vue/entete.html.php";

//fonction qui recupere toutes les categories
$categorie = getCategorie();

//si le formulaire envoie une categorie alors on recupere l'idCategorie et on recupere les sous categorie qui lui sont rattache
if (isset($_POST["categorie"]))
{
  if (!empty($_POST["categorie"]))
  {
    $idCategorie = $_POST["categorie"];
    $souscategorie = getSouscatégoriebyCategorie($idCategorie);
  }
}

//si le formulaire envoie une sous categorie alors on recupere l'idCategorie et on recupere les items qui lui sont rattache
if (isset($_POST["souscategorie"]))
{
  if (!empty($_POST["souscategorie"]))
  {
    $idSousCategorie = $_POST["souscategorie"];
    $item = getItembyidSousCategorie($idSousCategorie);
  }
}

//affichage de la page
include "../vue/vueAjoutStaff.php";

//si le formulaire envoie une categorie alors on entre les donnes recupere precedemment dans un tableau
if (isset($_POST["categorie"]))
{
  if (!empty($_POST["categorie"]))
  {
    $souscategorie = getSouscatégoriebyCategorie($idCategorie);
  }
}

//si le formulaire envoie une sous categorie alors on entre les donnes recupere precedemment dans un tableau
if (isset($_POST["souscategorie"]))
{
  if (!empty($_POST["souscategorie"]))
  {
    $idSousCategorie = $_POST["souscategorie"];
    $verif1 = true;
    $item = getItembyidSousCategorie($idSousCategorie);
  }
}

//si le formulaire envoie un item c'est que l'item a bien ete ajoute dans le creneaux
if (isset($_POST["item"]))
{
  if (!empty($_POST["item"]))
  {
    $idItem = $_POST["item"];
    print "L'item du créneau a bien été enregistré";
  }
}

//si le formulaire envoie 'newCateg' alors on le recupere et on l'affiche
if (isset($_POST["newCateg"]))
{
  $newCateg = $_POST["newCateg"];
  print $newCateg;
}

//affichage du pied de page
include "../vue/pied.html.php";

?>
