<?php
//permet de rediriger un joueur vers la page d'accueil
if (!isset($_SESSION))
{
    session_start();
}
include_once "protectionPage.php";
include_once "protectionStaff.php";
include_once "../modele/bd.joueur.inc.php";
$idMembre = $_POST['idMembre'];
$unMembre = getJoueurByIdMembre($idMembre);

if (isset($_POST['confirmation']))
{
  if ($_POST['confirmation']==1)
  {
    if(isset($_SESSION['token']) && isset($_SESSION['token_time']) && isset($_POST['token']))
    {
    	if($_SESSION['token'] == $_POST['token'])
    	{
    		$timestamp_ancien = time() - (60*60);
    		if($_SESSION['token_time'] >= $timestamp_ancien)
    		{
          supprMembre($idMembre);
          header('Location: pageAdmin.php');
        }
        else
        {
          session_unset();
          header('Location: connexion.php');
        }
      }
      else
      {
        session_unset();
        header('Location: connexion.php');
      }
    }
    else
    {
      session_unset();
      header('Location: connexion.php');
    }
  }
}
else
{
  include_once "../vue/vueConfirmationSuppressionMembre.php";
}

?>
