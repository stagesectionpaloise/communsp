<?php
//on verifie si il y a une session et si il y en a pas on demarre la session
if (!isset($_SESSION))
{
    session_start();
}
include_once "protectionPage.php";
include_once "protectionStaff.php";
//appel de la page modéle pour utiliser les fonctions
include_once "../modele/bd.joueur.inc.php";
$titre = "Accueil";

//appel des différentes fonctions pour aller chercher les données dans la base de données en fonction de l'idSituation
$listeJoueurApte = getListeJoueur(1);
$listeJoueurPro = getListeJoueur(2);
$listeJoueurBlesse = getListeJoueur(3);
$listeJoueurEcole = getListeJoueur(4);
$listeJoueurAutre = getListeJoueur(5);

$nbJoueurApte = getNombreJoueurBySituation(1);
$nbJoueurPro = getNombreJoueurBySituation(2);
$nbJoueurBlesse = getNombreJoueurBySituation(3);
$nbJoueurEcole = getNombreJoueurBySituation(4);
$nbJoueurAutre = getNombreJoueurBySituation(5);

//appel des pages pour l'affichage
include "../vue/entete.html.php";
include "../vue/vuePageAccueil.php";
include "../vue/pied.html.php";
 ?>
