<?php
//on verifie si il y a une session et si il y en a pas on demarre la session
if (!isset($_SESSION))
{
    session_start();
}
include_once "protectionPage.php";
//appel de la page modéle pour utiliser les fonctions
include_once "../modele/bd.joueur.inc.php";

$idMembre = $_SESSION["idMembre"];
$unJoueur = getJoueurByIdMembre($idMembre);
$titre = "Mon compte";

//appel des pages pour l'affichage
include "../vue/entete.html.php";
include "../vue/vueMonCompte.php";
include "../vue/pied.html.php";
?>
