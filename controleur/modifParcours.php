<?php
//on verifie si il y a une session et si il y en a pas on demarre la session
if (!isset($_SESSION))
{
    session_start();
}
include_once "protectionPage.php";
include_once "protectionStaff.php";

//appel de la page modéle pour utiliser les fonctions
include_once "../modele/bd.joueur.inc.php";

if(isset($_POST['ajout']))
{
  if($_POST['ajout']==1)
  {
    //récupére les données du compte si ils ne sont pas changés
    $idMembre = $_POST["idMembre"];
    $libParcours = $_POST["parcours"];

    addParcours($idMembre, $libParcours);
    header('Location: pageAccueil.php');
  }
}

if(isset($_POST['suppression']))
{
  if($_POST['suppression']==1)
  {
    //récupére les données du compte si ils ne sont pas changés
    $idParcours = $_POST["idParcours"];

    supprParcours($idParcours);
    header('Location: pageAccueil.php');
  }
}
?>
