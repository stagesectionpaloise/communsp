<?php
//on verifie si il y a une session et si il y en a pas on demarre la session
if (!isset($_SESSION))
{
    session_start();
}
//appel du fichier pour empecher a des personnes non identifie
//d'acceder a cette page
include_once "protectionPage.php";

//appel de la page modéle pour utiliser les fonctions
include_once "../modele/bd.seance.inc.php";
include_once "../modele/creSeance.inc.php";

//recuperation de la date d'aujourd'hui, du mois dernier et de la semaine derniere
$today = date('Y-m-d');
$lastMonth = mktime(0, 0, 0, date("Y"), date("m")-1, date("d"));
$lastWeek = mktime(0, 0, 0, date("Y"), date("m"), date("d")-8);

//si aucune periode n'est selectionne alors on selectionne toutes les seances
if (isset($_POST["periodeSeance"]))
{
  $periodeSeance = $_POST["periodeSeance"];
}
else
{
  $periodeSeance = 0;
}

//si le statut est celui d'un entraineur alors on recupere tous les creneaux
//sinon on recupere que les creneaux qui concerne le joueur
if ($_SESSION["statut"]<=1)
{
  $listeTousSeance = getSeance();
  $listeSeanceSemaineDerniere = getSeanceSemaineDerniere($today);
  $listeSeanceMoisDernier = getSeanceMoisDernier($today);
  $listeSeanceSemaineProchaine = getSeanceSemaineProchaine($today);
}
else
{
  //Récupération de l'id joueur
  $idMembre = $_SESSION["idMembre"];

  //Liste Seance si joueur connecté
  $listeTousSeance = getSeanceByIdMembre($idMembre);
  $listeSeanceSemaineDerniere = getSeanceSemaineDerniereByIdMembre($idMembre, $today);
  $listeSeanceMoisDernier = getSeanceMoisDernierByIdMembre($idMembre, $today);
  $listeSeanceSemaineProchaine = getSeanceSemaineProchaineByIdMembre($idMembre, $today);
}


$listeSeance = $listeTousSeance;

//appel des pages pour l'affichage
include "../vue/entete.html.php";
include "../vue/vueListeSeance.php";
include "../vue/pied.html.php";
?>
