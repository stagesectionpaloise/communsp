<?php
//on verifie si il y a une session et si il y en a pas on demarre la session
  if (!isset($_SESSION))
  {
      session_start();
  }
  //permet de rediriger une personne non-connecté vers la page de connexion pour éviter les attaques
  include_once "../modele/authentification.inc.php";

  $connecte = isLoggedOn();

  if($connecte == false)
  {
    header('Location: connexion.php');
  }

?>
