<?php
//on verifie si il y a une session et si il y en a pas on demarre la session
if (!isset($_SESSION))
{
    session_start();
}

//appel du fichier pour empecher a des personnes non identifie
//d'acceder a cette page
include_once "protectionPage.php";
//appel du fichier pour empecher une personne identifié non staff d'acceder a la page
include_once "protectionStaff.php";

//appel de la page modéle pour utiliser les fonctions
include_once "../modele/bd.joueur.inc.php";

//recuperation de l'idMembre grace au formulaire
$idMembre = $_POST["idMembre"];

//recuperation des donnees d'un joueur grace a une fonction
$unJoueur = getJoueurByIdMembre($idMembre);
//recuperation des parcours d'un joueur grace a son idMembre
$listeParcours = getParcoursByIdMembre($idMembre);

//titre pour l'onglet
$titre = "Ajout parcours";

//appel des pages pour l'affichage
include "../vue/entete.html.php";
include "../vue/vueAjoutParcours.php";
include "../vue/pied.html.php";
 ?>
