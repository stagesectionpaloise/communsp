<?php
//on verifie si il y a une session et si il y en a pas on demarre la session
if (!isset($_SESSION))
{
    session_start();
}
include_once "protectionPage.php";
include_once "../modele/bd.joueur.inc.php";

if(isset($_SESSION['token']) && isset($_SESSION['token_time']) && isset($_POST['token']))
{
	if($_SESSION['token'] == $_POST['token'])
	{
		$timestamp_ancien = time() - (60*60);
		if($_SESSION['token_time'] >= $timestamp_ancien)
		{
      if ($_SESSION['password'] == hash('sha1',$_POST['ancienMDP']) && $_POST["newMDP"] == $_POST["conNewMDP"])
      {
        //récupére les données du compte si ils ne sont pas changés
        $idMembre = $_POST["idMembre"];
        $ancienMDP = $_POST["ancienMDP"];
        $newMDP = $_POST["newMDP"];
        $conNewMDP = $_POST["conNewMDP"];
        $mdp = hash('sha1', $newMDP);

        updateMDP($mdp, $idMembre);

        header('Location: monCompte.php');
      }
      else
      {
        header('Location: monCompte.php');
      }
		}
    else
    {
      session_unset();
      header('Location: connexion.php');
    }
  }
  else
  {
    session_unset();
    header('Location: connexion.php');
  }
}
else
{
  session_unset();
  header('Location: connexion.php');
}

?>
