<?php
//on verifie si il y a une session et si il y en a pas on demarre la session
if (!isset($_SESSION))
{
    session_start();
}

//appel du fichier pour empecher a des personnes non identifie
//d'acceder a cette page
include_once "protectionPage.php";
//appel du fichier pour empecher une personne identifié non staff d'acceder a la page
include_once "protectionStaff.php";

//appel de la page modéle pour utiliser les fonctions
include_once "../modele/creSeance.inc.php";

//affichage de l'entete
include "../vue/entete.html.php";

//si on recupere des donnees en get on les mets dans des variables
if (isset($_GET["Ladate"]))
{
  $idSeance = $_GET['idSeance'];
  $DateEntr = $_GET["Ladate"];
  $heureDebut = $_GET["heureDebut"];
  $heureFin = $_GET["heureFin"];
  print $idSeance;
}

//si on recupere des donnees en post on les mets dans des variables
if (isset($_POST["Ladate"]))
{
  $DateEntr = $_POST["Ladate"];
  $heureDebut = $_POST["heureDebut"];
  $heureFin = $_POST["heureFin"];
  $idSeance = $_POST["idSeance"];
  print $idSeance;
}

//recuperation des entraineur, des categories et des joueurs
$entraineurs = getEntraineurByStatus();
$categorie = getCategorie();
$membres = getMembresbyStatut();

//recuperation des creneaux d'une seance
$touscreneaux = getTousCreneauxbyIdSeance($idSeance);

//declaration des tableaux
$idCre = array();
$duree = array();
$tabItem = array();

//entree des donnees dans les differents tableaux
for ($i = 0; $i < count($touscreneaux); $i++)
{
  array_push($idCre, $touscreneaux[$i]["idCreneaux"]);
  array_push($tabItem, $touscreneaux[$i]["idItem"]);
  array_push($duree, $touscreneaux[$i]["duree"]);
}

//declaration des tableaux
$tabIdSousCateg = array();
$var = array();
$libelItem = array();

//entree des donnees dans les differents tableaux
for ($i = 0; $i < count($tabItem); $i++)
{
  $idItem = $tabItem[$i];
  $var = getSousCategbyidItem($idItem);
  array_push($libelItem, $var["libItem"]);
  array_push($tabIdSousCateg, $var["idSousCategorie"]);
}

//declaration des tableaux
$tabLibelSousCateg = array();
$vor = array();
$tabIdCateg = array();

//entree des donnees dans les differents tableaux
for ($i = 0; $i < count($tabIdSousCateg); $i++)
{
  $idSousCategorie = $tabIdSousCateg[$i];
  $vor = getLibelSousCategByIdSousCateg($idSousCategorie);
  array_push($tabLibelSousCateg, $vor["libSousCategorie"]);
  array_push($tabIdCateg, $vor["idCategorie"]);
}

//declaration des tableaux
$tabLibelCateg = array();
$vir = array();

//entree des donnees dans les differents tableaux
for ($i = 0; $i < count($tabIdCateg); $i++)
{
  $idCategorie = $tabIdCateg[$i];
  $vir = getLibelCategoriebyIdSousCateg($idCategorie);
  array_push($tabLibelCateg, $vir["libCategorie"]);
}

?>

<!-- affichage du titre de la page -->
<h1 class="titre">Entrainement du : <?php echo($DateEntr) ?> </h1>

<?php
//affichage de la page et du pied
include "../vue/vueCreationCreneau.php";
include "../vue/pied.html.php";


$verif = false;
$creneau = false;
if (isset($_POST["entraineur"])&&isset($_POST["duree"]))
{
    if (!empty($_POST["entraineur"])&& !empty($_POST["duree"]))
    {
      //verification du token et verification de sa duree
      //il s'efface au bout d'une heure et deconnecte la personne
      if(isset($_SESSION['token']) && isset($_SESSION['token_time']) && isset($_POST['token']))
      {
      	if($_SESSION['token'] == $_POST['token'])
      	{
          $timestamp_ancien = time() - (60*60);
      		if($_SESSION['token_time'] >= $timestamp_ancien)
      		{
            //recuperation des donnees du formulaire
            $duree = $_POST["duree"];
            $entraineursPresent = $_POST["entraineur"];
            //recuperation des joueurs present sur le creneau
            if (isset($_POST["joueur"]))
            {
              $joueursPresent = $_POST["joueur"];
            }
            //ajout du creneau
            $ret = addCreneau($idSeance, $duree);
            //recuperation de tous les creneaux afin de recuperer le dernier entre
            $listeCreneaux = getTousCreneaux();
            $tailleCreneaux = count($listeCreneaux)-1;
            //recuperation de l'idCreneau du dernier creneau entre
            $idCreneau = $listeCreneaux[$tailleCreneaux]['idCreneaux'];

            //si on a u  retour alors le creneau est bien entre
            if ($ret)
            {
              $creneau = true;
              $verif = true;
              print "creneau envoyé";
              //on ajoute tous les membres dans la table participer
              for ($i = 0; $i < count($membres); $i++)
              {
                addParticiper($membres[$i]['idMembre'], $idCreneau);
                //si un joueur fait est dans la table joueur present alors on met sa presence a 1 sinon on l'a met a 0
                if (isset($_POST["joueur"]))
                {
                  if (in_array($membres[$i]['idMembre'], $joueursPresent)==true)
                  {
                    modificationPresent($membres[$i]['idMembre'], $idCreneau, 1);
                  }
                  else
                  {
                    modificationPresent($membres[$i]['idMembre'], $idCreneau, 0);
                  }
                }
                else
                {
                  modificationPresent($membres[$i]['idMembre'], $idCreneau, 0);
                }
              }
              //si un entraineur fait est dans la table entraineur present alors on met sa presence a 1 sinon on l'a met a 0
              for ($i = 0;$i < count($entraineurs); $i++)
              {
                addParticiper($entraineurs[$i]['idMembre'], $idCreneau);
                if (isset($_POST["entraineur"]))
                {
                  if (in_array($entraineurs[$i]['idMembre'], $entraineursPresent)==true)
                  {
                    modificationPresent($entraineurs[$i]['idMembre'], $idCreneau, 1);
                  }
                  else
                  {
                    modificationPresent($entraineurs[$i]['idMembre'], $idCreneau, 0);
                  }
                }
                else
                {
                  modificationPresent($entraineurs[$i]['idMembre'], $idCreneau, 0);
                }
              }
             }
             else
             {
                $msg = "l'entrainement n'a pas été enregistré.";
            }
          }
          else
          {
            //fonction qui enleve la session en cours
            session_unset();
            //redirection vers la page de connexion
            header('Location: connexion.php');
          }
    		}
        else
        {
          session_unset();
          header('Location: connexion.php');
        }
    	}
      else
      {
        session_unset();
        header('Location: connexion.php');
      }
    }
}
else
{
  print 'Veuillez remplir tous les champs';
}

//efface le creneau si il le form renvoie 'deleteCreneau'
if (isset($_POST["DeleteCreneau"]))
{
  $idCreneau = $_POST['idCreneau'];
  supprimerCreneau($idCreneau);
}


if ($verif == true)
{
  ?>
  <form action="typeCreneaux.php" type="POST">
    <input type="hidden" value="<?php echo $idCreneau ?>" name="idCreneau"/>
    <input type="hidden" value="<?php echo $idSeance ?>" name="idSeance"/>
    <input type="submit"/>
  </form>
  <?php
}
