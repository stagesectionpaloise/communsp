<?php
include_once "../modele/bd.poids.inc.php";
$titre = "Visualisation morpho";
//permet de rediriger un joueur vers la page d'accueil
if (!isset($_SESSION))
{
    session_start();
}

include_once "protectionPage.php";

if (isset($_POST["ajout"]))
{
  if ($_POST["ajout"]==1)
  {
    if(isset($_SESSION['token']) && isset($_SESSION['token_time']) && isset($_POST['token']))
    {
    	if($_SESSION['token'] == $_POST['token'])
    	{
    		$timestamp_ancien = time() - (60*60);
    		if($_SESSION['token_time'] >= $timestamp_ancien)
    		{
          $idMembre = $_POST["idMembre"];
          $dateMorpho = $_POST["dateMorpho"];
          $poids = $_POST["poids"];
          $taille = $_POST["taille"];
          $masseGraisseuse = $_POST["masseGraisseuse"];

          if ($masseGraisseuse == "")
          {
            $masseGraisseuse = NULL;
          }

          if ($taille == "")
          {
            $taille = NULL;
          }

          if(isset($_FILES['file']))
          {
            $tmpName = $_FILES['file']['tmp_name'];
            $name = $_FILES['file']['name'];
            move_uploaded_file($tmpName, '../photoJoueurPoids/'.$name);
          }
          addPoids($dateMorpho, $idMembre, $poids, $taille, $masseGraisseuse, $name);
    		}
        else
        {
          session_unset();
          header('Location: connexion.php');
        }
  		}
      else
      {
        session_unset();
        header('Location: connexion.php');
      }
  	}
    else
    {
      session_unset();
      header('Location: connexion.php');
    }
  }
}

$idMembre = $_POST["idMembre"];

$listePoids = getPoidsByIdMembre($idMembre);
$unJoueur = getJoueurByIdMembre($idMembre);
?>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript">
  google.charts.load("current", {packages:["corechart"]});
  google.charts.setOnLoadCallback(drawChart);
  //fonction de dessin du graphe
  function drawChart()
  {
    var data = google.visualization.arrayToDataTable([
      ['Date', 'Poids (en kg)']
      <?php
        //récuperation des différents créneaux
        for ($i = 0; $i < count($listePoids); $i++)
        {
          echo ",['";
          echo $listePoids[$i]['dateMorpho'];
          echo "',Number(";
          echo $listePoids[$i]['poids'];
          echo ")]";
        }

       ?>
    ]);

    var options = {
      title: 'Poids',
      pieHole: 0.4,
      backgroundColor: 'transparent',
      colors: ['#024638']
    };

    var chart = new google.visualization.LineChart(document.getElementById('graphePoidsDate'));
    chart.draw(data, options);
  }
</script>

<?php
//appel des pages pour l'affichage
include "../vue/entete.html.php";
include "../vue/vueVisualisationPoids.php";
include "../vue/pied.html.php";
?>
