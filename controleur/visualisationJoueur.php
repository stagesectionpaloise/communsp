<?php
//permet de rediriger un joueur vers la page d'accueil
if (!isset($_SESSION))
{
    session_start();
}
include_once "protectionPage.php";
//appel de la page modéle pour utiliser les fonctions
include_once "../modele/bd.joueur.inc.php";

//changement d'affichage si l'on est staff ou joueur
if ($_SESSION["statut"]<=1)
{
  //récupération de l'idMembre quand le staff accéde à la page via la page d'accueil
  $idMembre = $_POST["idMembre"];
}
else
{
  $idMembre = $_SESSION["idMembre"];
}

$titre = "Visualisation du joueur";

//appel des différentes fonctions pour aller chercher les données dans la base de données
$unJoueur = getJoueurByIdMembre($idMembre);
$listeParcours = getParcoursByIdMembre($idMembre);
$listeSelection = getSelectionByIdMembre($idMembre);
$listeCreneaux = getCreneauxByIdMembre($idMembre);
$unPoste = getPosteByIdMembre($idMembre);
$unPoste2 = getPoste2ByIdMembre($idMembre);
$listeCategories = getCategorie();
if(!isset($_POST["periode"]))
{
  $lesCategories = getCategorieByIdMembre($idMembre);
  if(isset($_POST["categorie"]))
  {
    $idCategorie = $_POST["categorie"];
    $listeSousCategories = getSousCategorieByIdCategorie($idCategorie);
    $lesSousCategories = getSousCategorieByCategorieByIdMembre($idMembre, $idCategorie);
    if(isset($_POST["souscategorie"]))
    {
      $idSousCategorie = $_POST["souscategorie"];
      $lesItems = getItemBySousCategorieByIdMembre($idMembre, $idSousCategorie);
    }
  }
}
else
{
  if($_POST["periode"]==0)
  {
    $unMois = $_POST["periode"];
    $lesCategories = getCategorieByIdMembre($idMembre);
    if(isset($_POST["categorie"]))
    {
      $idCategorie = $_POST["categorie"];
      $listeSousCategories = getSousCategorieByIdCategorie($idCategorie);
      $lesSousCategories = getSousCategorieByCategorieByIdMembre($idMembre, $idCategorie);
      if(isset($_POST["souscategorie"]))
      {
        $idSousCategorie = $_POST["souscategorie"];
        $lesItems = getItemBySousCategorieByIdMembre($idMembre, $idSousCategorie);
      }
    }
  }
  else
  {
    $unMois = $_POST["periode"];
    $lesCategories = getCategorieByIdMembreByMois($idMembre, $unMois);
    if(isset($_POST["categorie"]))
    {
      $idCategorie = $_POST["categorie"];
      $listeSousCategories = getSousCategorieByIdCategorie($idCategorie);
      $lesSousCategories = getSousCategorieByCategorieByIdMembreByMois($idMembre, $idCategorie, $unMois);
      if(isset($_POST["souscategorie"]))
      {
        $idSousCategorie = $_POST["souscategorie"];
        $lesItems = getItemBySousCategorieByIdMembreByMois($idMembre, $idSousCategorie, $unMois);
      }
    }
  }
}



?>

<!-- script javascript pour afficher le graphe camembert -->
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript">
  google.charts.load("current", {packages:["corechart"]});
  google.charts.setOnLoadCallback(drawChart);
  //fonction de dessin du graphe
  function drawChart()
  {
    var data = google.visualization.arrayToDataTable([
      ['Catégorie', 'Minutes']
      <?php
        //récuperation des différents créneaux
        for ($i = 0; $i < count($lesCategories); $i++)
        {
          echo ",['";
          echo $lesCategories[$i]['libCategorie'];
          echo "',Number(";
          echo $lesCategories[$i]['sommeDuree'];
          echo ")]";
        }

       ?>
    ]);

    var options = {
      title: 'Categorie',
      pieHole: 0.4,
      backgroundColor: 'transparent',
      colors: ['#006400', '#008000', '#32CD32', '#7CFC00', '#ADFF2F']
    };

    var chart = new google.visualization.PieChart(document.getElementById('grapheCategorie'));
    chart.draw(data, options);
  }
</script>

<script type="text/javascript">
  google.charts.load("current", {packages:["corechart"]});
  google.charts.setOnLoadCallback(drawChart);
  //fonction de dessin du graphe
  function drawChart()
  {
    var data = google.visualization.arrayToDataTable([
      ['Type entrainement', 'Minutes']
      <?php
        //récuperation des différents créneaux
        for ($i = 0; $i < count($lesSousCategories); $i++)
        {

          echo ",['";
          echo $lesSousCategories[$i]['libSousCategorie'];
          echo "',Number(";
          echo $lesSousCategories[$i]['sommeDuree'];
          echo ")]";

        }

       ?>
    ]);

    var options = {
      title: 'Sous categorie',
      pieHole: 0.4,
      backgroundColor: 'transparent',
      colors: ['#006400', '#008000', '#32CD32', '#7CFC00', '#ADFF2F']
    };

    var chart = new google.visualization.PieChart(document.getElementById('grapheSousCategorie'));
    chart.draw(data, options);
  }
</script>

<script type="text/javascript">
  google.charts.load("current", {packages:["corechart"]});
  google.charts.setOnLoadCallback(drawChart);
  //fonction de dessin du graphe
  function drawChart()
  {
    var data = google.visualization.arrayToDataTable([
      ['Type entrainement', 'Minutes']
      <?php
        //récuperation des différents créneaux
        for ($i = 0; $i < count($lesItems); $i++)
        {

          echo ",['";
          echo $lesItems[$i]['libItem'];
          echo "',Number(";
          echo $lesItems[$i]['sommeDuree'];
          echo ")]";

        }

       ?>
    ]);

    var options = {
      title: 'Item',
      pieHole: 0.4,
      backgroundColor: 'transparent',
      colors: ['#006400', '#008000', '#32CD32', '#7CFC00', '#ADFF2F']
    };

    var chart = new google.visualization.PieChart(document.getElementById('grapheItem'));
    chart.draw(data, options);
  }
</script>

<?php
//appel des pages pour l'affichage
include "../vue/entete.html.php";
include "../vue/vueVisualisationJoueur.php";
include "../vue/pied.html.php";
 ?>
