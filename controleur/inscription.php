<?php
//on verifie si il y a une session et si il y en a pas on demarre la session
if (!isset($_SESSION))
{
    session_start();
}

//appel du fichier pour empecher a des personnes non identifie
//d'acceder a cette page
include_once "protectionPage.php";
//appel du fichier pour empecher une personne identifié non staff d'acceder a la page
include_once "protectionStaff.php";

//appel de la page modéle pour utiliser les fonctions
include_once "../modele/bd.utilisateur.inc.php";

$inscrit = false;
$msg="";
// recuperation des donnees GET, POST, et SESSION
if (isset($_POST["prenom"]) && isset($_POST["nom"]) && isset($_POST["username"]) && isset($_POST["password"]) && isset($_POST["statut"]))
{
    if ($_POST["prenom"] != "" && $_POST["nom"] != "" && $_POST["username"] != "" && $_POST["password"] != "" && $_POST["statut"] != "")
    {
      //verification du token et verification de sa duree
      //il s'efface au bout d'une heure et deconnecte la personne
      if(isset($_SESSION['token']) && isset($_SESSION['token_time']) && isset($_POST['token']))
      {
      	if($_SESSION['token'] == $_POST['token'])
      	{
      		$timestamp_ancien = time() - (60*60);
      		if($_SESSION['token_time'] >= $timestamp_ancien)
      		{
            //recuperation des donnees des formulaires
            $prenom = $_POST["prenom"];
            $nom = $_POST["nom"];
            $username = $_POST["username"];
            $password = $_POST["password"];
            $statut = $_POST["statut"];

            //enregistrement des donnees
            $ret = addUtilisateur($prenom, $nom, $username, $password, $statut);
            if ($ret)
            {
                $inscrit = true;
            }
            else
            {
                $msg = "l'utilisateur n'a pas été enregistré.";
            }
      		}
          else
          {
            //fonction qui enleve la session en cours
            session_unset();
            //redirection vers la page de connexion
            header('Location: connexion.php');
          }
    		}
        else
        {
          session_unset();
          header('Location: connexion.php');
        }
    	}
      else
      {
        session_unset();
        header('Location: connexion.php');
      }
  	}
}
else
{
  $msg="Renseigner tous les champs...";
}

//si le joueur est inscrit, on peut en ajouter un autre en etant redirige vers la page d'ajout des membres
if ($inscrit)
{
  header('Location: ajoutMembre.php');
}
?>
