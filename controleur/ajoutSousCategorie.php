<?php
//on verifie si il y a une session et si il y en a pas on demarre la session
if (!isset($_SESSION))
{
    session_start();
}

//appel du fichier pour empecher a des personnes non identifie
//d'acceder a cette page
include_once "protectionPage.php";
//appel du fichier pour empecher une personne identifié non staff d'acceder a la page
include_once "protectionStaff.php";

//appel de la page modéle pour utiliser les fonctions
include_once "../modele/creSeance.inc.php";

//titre pour l'onglet
$titre="Ajout Sous Categorie";

//fonction qui recupere toutes les categories
$categorie = getCategorie();

//appel des pages pour l'affichage
include "../vue/entete.html.php";
include "../vue/vueAjoutSousCategorie.php";
include "../vue/pied.html.php";
 ?>
