<?php
//on verifie si il y a une session et si il y en a pas on demarre la session
if (!isset($_SESSION))
{
    session_start();
}

//appel de la page modéle pour utiliser les fonctions
include_once "../modele/authentification.inc.php";

//titre pour l'onglet
$titre = "connexion";

// recuperation des donnees GET, POST, et SESSION
if (!isset($_POST["username"]) || !isset($_POST["password"]))
{
    // on affiche le formulaire de connexion
    $titre = "authentification";
    include "../vue/vueAuthentification.php";
}
else
{
    //recuperation des donnes du formulaire
    $username=$_POST["username"];
    $password=$_POST["password"];

    //fonction qui cree les valeurs de session et l'identifie
    login($username,$password);

    if (isLoggedOn())
    {
        // si l'utilisateur est connecté on redirige vers l'accueil
        $titre = "Connexion réussi";
        if ($_SESSION["statut"]<=1)
        {
          include "../controleur/pageAccueil.php";
        }
        else
        {
          include "../controleur/visualisationJoueur.php";
        }
    }
    else
    {
        // l'utilisateur n'est pas connecté, on affiche le formulaire de connexion
        $titre = "Connexion refusée";
        include "../vue/vueAuthentification.php";
    }
}

?>
