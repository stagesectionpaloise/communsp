<?php
//on verifie si il y a une session et si il y en a pas on demarre la session
if (!isset($_SESSION))
{
    session_start();
}
include_once "protectionPage.php";
include_once "protectionStaff.php";
$titre="Page admin";
//appel des pages pour l'affichage
include "../vue/entete.html.php";
include "../vue/vuePageAdmin.php";
include "../vue/pied.html.php";
 ?>
