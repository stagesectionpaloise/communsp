<?php
//permet de rediriger un joueur vers la page d'accueil
if (!isset($_SESSION))
{
    session_start();
}
include_once "protectionPage.php";
include_once "protectionStaff.php";
//appel de la page modéle pour utiliser les fonctions
include_once "../modele/bd.joueur.inc.php";

//récupération de l'idSituation et de l'idMembre pour modifier la situation d'un joueur
$idSituation = $_POST["idSituation"];
$idMembre = $_POST["idMembre"];

//appel de la fonction
updateSituationJoueur($idSituation, $idMembre);

//redirection vers la page d'accueil
header('Location: pageAccueil.php');

?>
