<?php
//on verifie si il y a une session et si il y en a pas on demarre la session
if (!isset($_SESSION))
{
    session_start();
}

//appel du fichier pour empecher a des personnes non identifie
//d'acceder a cette page
include_once "protectionPage.php";
//appel du fichier pour empecher une personne identifié non staff d'acceder a la page
include_once "protectionStaff.php";

//appel de la page modéle pour utiliser les fonctions
include_once "../modele/creSeance.inc.php";

//appel des pages pour l'affichage
include "../vue/entete.html.php";
include "../vue/vueCreationSeance.php";
include "../vue/pied.html.php";

// on récupére les données entrées et on les stock dans des variables
// recuperation des donnees GET, POST, et SESSION
$seance = false;
$verification = true;
if (isset($_POST["Ladate"]) && isset($_POST["heureDebut"]) && isset($_POST["heureFin"]))
{
  if (!empty($_POST["Ladate"]) && !empty($_POST["heureDebut"]) && !empty($_POST["heureFin"]))
  {
    //verification du token et verification de sa duree
    //il s'efface au bout d'une heure et deconnecte la personne
    if(isset($_SESSION['token']) && isset($_SESSION['token_time']) && isset($_POST['token']))
    {
    	if($_SESSION['token'] == $_POST['token'])
    	{
        $timestamp_ancien = time() - (60*60);
    		if($_SESSION['token_time'] >= $timestamp_ancien)
    		{
          //recuperation des donnees du form
          $laDate = date('Y-m-d');
          $laDate = $_POST["Ladate"];
          $heureDebut = $_POST["heureDebut"];
          $heureFin = $_POST["heureFin"];
          $laDateVerif = strval($laDate);

          // $listeSeance = getTousSeance();
          // $tailleSeance = count($listeSeance)-1;
          // $idSeance = $listeSeance[$tailleSeance]['idSeance'];


          if ($verification == true)
          {
            //fonction qui ajoute la seance
            $ret = addSeance($laDate, $heureDebut, $heureFin);
            if ($ret)
            {
              $seance = true;
            }
            else
            {
              $msg = "l'entrainement n'a pas été enregistré.";
            }
          }
    		}
        else
        {
          //fonction qui enleve la session en cours
          session_unset();
          //redirection vers la page de connexion
          header('Location: connexion.php');
        }
  		}
      else
      {
        session_unset();
        header('Location: connexion.php');
      }
  	}
    else
    {
      session_unset();
      header('Location: connexion.php');
    }
	}
}
else
{
  echo ("<br />");
  echo("Veuillez renseigner tous les champs...");
}
// if ($seance == true) {
//   echo "<a href='creationCreneau.php?Ladate=".$laDate."& heureDebut=".$heureDebut."& heureFin=".$heureFin."& idSeance=".$idSeance." '>Continuer vers la création de crenau</a>";
// }
?>
