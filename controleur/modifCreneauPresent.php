<?php
//on verifie si il y a une session et si il y en a pas on demarre la session
if (!isset($_SESSION))
{
    session_start();
}

include_once "../modele/creSeance.inc.php";

$idCreneau = $_POST["idCreneau"];
$idSeance = $_POST['idSeance'];


$entraineursPresent = getMembresbyPresent($idCreneau, 1);
$joueursPresent = getMembresbyPresent($idCreneau, 2);


include_once "protectionPage.php";
include_once "protectionStaff.php";
include "../vue/entete.html.php";
include "../vue/vueModifCreneauPresent.php";
include "../vue/pied.html.php";



for ($i = 0; $i < count($joueursPresent); $i++)
{
  if (isset($_POST["joueur"]))
  {
    $joueursCoche = $_POST["joueur"];
    if (in_array($joueursPresent[$i]['idMembre'], $joueursCoche)==true)
    {
      modificationPresent($joueursPresent[$i]['idMembre'], $idCreneau, 0);
    }
    else
    {
      modificationPresent($joueursPresent[$i]['idMembre'], $idCreneau, 1);
    }
  }
  else
  {
    modificationPresent($joueursPresent[$i]['idMembre'], $idCreneau, 1);
  }
}
for ($i = 0;$i < count($entraineursPresent); $i++)
{
  if (isset($_POST["entraineur"]))
  {
    $entraineursCoche = $_POST["entraineur"];
    if (in_array($entraineursPresent[$i]['idMembre'], $entraineursCoche)==true)
    {
      modificationPresent($entraineursPresent[$i]['idMembre'], $idCreneau, 0);
    }
    else
    {
      modificationPresent($entraineursPresent[$i]['idMembre'], $idCreneau, 1);
    }
  }
  else
  {
    modificationPresent($entraineursPresent[$i]['idMembre'], $idCreneau, 1);
  }
}
