<?php
//on verifie si il y a une session et si il y en a pas on demarre la session
if (!isset($_SESSION))
{
    session_start();
}

//appel de la page modéle pour utiliser les fonctions
include_once "../modele/creSeance.inc.php";

//recuperation des entraineur et des joueurs
$entraineurs = getEntraineurByStatus();
$membres = getMembresbyStatut();
$idCreneau = $_POST["idCreneau"];
$idSeance = $_POST['idSeance'];

//appel du fichier pour empecher a des personnes non identifie
//d'acceder a cette page
include_once "protectionPage.php";
//appel du fichier pour empecher une personne identifié non staff d'acceder a la page
include_once "protectionStaff.php";

//appel des pages pour l'affichage
include "../vue/entete.html.php";
include "../vue/vueModifCreneau.php";
include "../vue/pied.html.php";
