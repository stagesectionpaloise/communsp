<!-- <!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"> -->
<link rel="stylesheet" type="text/css" href="../css/styleEntete.css"/>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title><?php echo $titre ?></title>
        <link href="https://fonts.googleapis.com/css?family=Lobster" rel="stylesheet">
    </head>
          <nav>
            <ul id="menuGeneral">
              <li><a href="../controleur/monCompte.php" >Mon compte</a></li>
              <li><a href="../controleur/listeSeance.php">Liste des seances</a></li>
              <li id="logo"><a href="
                <?php
                  if($_SESSION["statut"]<=1)
                  {
                    echo "../controleur/pageAccueil.php";
                  }
                  else
                  {
                    echo "../controleur/visualisationJoueur.php";
                  }
                ?>
                "><img src="../css/Logo Section 2021.png" alt="logo" /></a></li>
              <?php
                if($_SESSION["statut"]<=1)
                {
                  echo "<li><a href='../controleur/creationSeance.php'>Creation seance</a></li>";
                  echo "<li><a href='../controleur/pageAdmin.php'>Page admin</a></li>";
                }
              ?>
  		</ul>
    </nav>
