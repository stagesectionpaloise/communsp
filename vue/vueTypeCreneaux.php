<link rel="stylesheet" href="../css/styleTypeCreneaux.css">

<h3 class=titreBis>Choisissez le type de l'entrainement</h3>

<form action="../controleur/typeCreneaux.php" method="POST">

    <label name="categorie-select">selectionner une catégorie :</label>

    <select name="categorie" id="categorie-select">
      <?php
      for ($i = 0; $i < count($categorie); $i++)
      {
        ?>
        <option value=<?php echo $categorie[$i]['idCategorie']?>><?php echo $categorie[$i]['libCategorie']?></option>;
<?php
      }
?>
  </select>
  <input type="hidden" value="<?php echo $idSeance ?>" name="idSeance"/>
  <input type="hidden" value="<?php echo $idCreneau ?>" name="idCreneau"/>
  <input type="submit"/>
</form>


<?php
if (isset($_POST["categorie"]) == true)
{
  ?>
  <form action="../controleur/typeCreneaux.php" method="POST">

      <label name="sousccategorie-select">selectionner une souscatégorie :</label>

      <select name="souscategorie" id="souscategorie-select">
        <?php

        for ($i = 0; $i < count($souscategorie); $i++)
        {
          ?>
          <option value=<?php echo $souscategorie[$i]['idSousCategorie']?>><?php echo $souscategorie[$i]['libSousCategorie']?></option>;
  <?php
        }
  ?>
    </select>
    <input type="hidden" value="<?php echo $idSeance ?>" name="idSeance"/>
    <input type="hidden" value="<?php echo $idCreneau ?>" name="idCreneau"/>
    <input type="submit"/>
  </form>
  <?php
}

if (isset($_POST["souscategorie"]) == true)
{
  ?>
  <form action="../controleur/typeCreneaux.php" method="POST">

      <label name="item-select">selectionner un item :</label>

      <select name="item" id="item-select">
        <?php

        for ($i = 0; $i < count($item); $i++)
        {
          ?>
          <option value=<?php echo $item[$i]['idItem']?>><?php echo $item[$i]['libItem']?></option>;
  <?php
        }
  ?>
    </select>
    <input type="hidden" value="<?php echo $idSeance ?>" name="idSeance"/>
    <input type="hidden" value="<?php echo $idCreneau ?>" name="idCreneau"/>
    <input type="submit"/>
  </form>
  <?php
}
