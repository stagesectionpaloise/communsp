<link rel="stylesheet" href="../css/styleCreationCreneau.css">

<h2>Modification des informations du creneau</h2>
<form action="../controleur/modifCreneauPresent.php" method="POST">
  <br/>
  <h3 class="titreBis">Modifier les joueurs présents sur le créneau</h3>
   <br />
   <input type='hidden' name='idCreneau' value='<?php echo $idCreneau ?>'/>
   <input type='hidden' name='idSeance' value='<?php echo $idSeance ?>'/>
   <input type='hidden' name='token' value='<?php echo $_SESSION['token']; ?>'>
   <input type="submit"/>
</form>



<form action="../controleur/modifCreneauAbsent.php" method="POST">
  <br/>
  <h3 class="titreBis">Modifier les joueurs absents sur le créneau</h3>
   <br />
   <input type='hidden' name='idCreneau' value='<?php echo $idCreneau ?>'/>
   <input type='hidden' name='idSeance' value='<?php echo $idSeance ?>'/>
   <input type='hidden' name='token' value='<?php echo $_SESSION['token']; ?>'>
   <input type="submit"/>
</form>
