<link href="../css/visualisationJoueur.css" rel="stylesheet" type="text/css">
<h1><?= $unJoueur[0]['nom']; ?> <?= $unJoueur[0]['prenom']; ?></h1>

<!-- affichage de la photo  -->
<img class="fit-picture"
     src="../photoJoueur/<?= $unJoueur[0]['lienPhoto']; ?>"
     alt="photoJoueur">

<!-- affichage des données du joueur -->
<table class = "infoJoueur">

  <?php
    if (isset($unPoste[0]['idPoste']))
    {
      echo "<tr>";
      echo "<td>Poste 1</td>";
      echo "<td>";
      echo $unPoste[0]['libPoste'];
      echo "</td>";
      echo "</tr>";
    }
    if (isset($unPoste2[0]['idPoste']))
    {
      echo "<tr>";
      echo "<td>Poste 2</td>";
      echo "<td>";
      echo $unPoste2[0]['libPoste'];
      echo "</td>";
      echo "</tr>";
    }
  ?>

  <tr>
   <td>Date de naissance</td>
   <td><?= $unJoueur[0]['dateNaissance']; ?></td>
  </tr>

  <tr>
   <td>Numéro de téléphone</td>
   <td><?= $unJoueur[0]['tel']; ?></td>
  </tr>

  <tr>
   <td>Adresse mail</td>
   <td><?= $unJoueur[0]['mail']; ?></td>
  </tr>

  <tr>
   <td>Pays</td>
   <td><?= $unJoueur[0]['pays']; ?></td>
  </tr>

  <?php
    if ($_SESSION["statut"]<=1)
    {
   ?>
    <tr>
      <td>JIFF</td>
      <td>
        <?php
          if($unJoueur[0]['JIFF']==0)
          {
            echo "Non";
          }
          else
          {
            echo "Oui";
          }
        ?>
      </td>
    </tr>

  <?php
    if (isset($unJoueur[0]['dureeContrat']))
    {
      echo "<tr>";
      echo "<td>Durée de contrat</td>";
      echo "<td>";
      echo $unJoueur[0]['dureeContrat'];
      echo "</td>";
      echo "</tr>";
    }
  }
?>

</table>



<form action='../controleur/visualisationPoids.php' method='post'>
  <input type='hidden' name='idMembre' value='<?php echo $idMembre ?>;'>
  <input class= 'boutonMorpho' type='submit' value='Visualiser la morpho'>
</form>

<?php
  if ($_SESSION["statut"]<=1)
  {
 ?>
  <form action='../controleur/modifierInfoMembre.php' method='post'>
    <input type='hidden' name='idMembre' value='<?php echo $idMembre ?>;'>
    <input type='hidden' name='prenom' value='<?php echo $unJoueur[0]['prenom'] ?>;'>
    <input type='hidden' name='nom' value='<?php echo $unJoueur[0]['nom'] ?>;'>
    <input type='hidden' name='username' value='<?php echo $unJoueur[0]['username'] ?>;'>
    <input type='hidden' name='mail' value='<?php echo $unJoueur[0]['mail'] ?>;'>
    <input type='hidden' name='tel' value='<?php echo $unJoueur[0]['tel'] ?>;'>
    <input type='hidden' name='dateNaissance' value='<?php echo $unJoueur[0]['dateNaissance'] ?>;'>
    <input type='hidden' name='pays' value='<?php echo $unJoueur[0]['pays'] ?>;'>
    <?php
    if (isset($unPoste[0]['idPoste']))
    {
    ?>
       <input type='hidden' name='poste' value='<?php echo $unPoste[0]['idPoste'] ?>;'>
    <?php
    }
    if (isset($unPoste2[0]['idPoste2']))
    {
    ?>
       <input type='hidden' name='poste2' value='<?php echo $unPoste2[0]['idPoste2'] ?>;'>
    <?php
    }
    ?>

    <input type='submit' value='Modifier les infos joueur'>
  </form>
<?php
  }
?>

<!-- affichage du parcours -->
<h2>Parcours : </h2>

<p>

  <?php
    //boucle pour voir tous les parcours d'un joueur
    for ($i = 0; $i < count($listeParcours); $i++)
    {
      echo $listeParcours[$i]['libParcours'];
  ?>
    <br />
  <?php
    }
   ?>
</p>
<?php
  if ($_SESSION["statut"]<=1)
  {
 ?>
    <form action='../controleur/ajoutParcours.php' method='post'>
      <input type='hidden' name='idMembre' value='<?php echo $idMembre ?>'>
      <input class= 'boutonMorpho' type='submit' value='Modif parcours'>
    </form>
<?php
  }
 ?>

<br /><br />

<!-- affichage du sélection -->
<h2>Sélections:</h2>
<p>

  <?php
  //boucle pour voir toutes les sélections d'un joueur
    for ($i = 0; $i < count($listeSelection); $i++)
    {
      echo $listeSelection[$i]['libSelection'];
  ?>
    <br />
  <?php
    }
   ?>
</p>
<?php
  if ($_SESSION["statut"]<=1)
  {
 ?>
    <form action='../controleur/ajoutSelection.php' method='post'>
      <input type='hidden' name='idMembre' value='<?php echo $idMembre ?>'>
      <input class= 'boutonMorpho' type='submit' value='Modif sélection'>
    </form>
<?php
  }
 ?>

<br /><br />

<h2>Entraînement</h2>
<form action="../controleur/visualisationJoueur.php" method="post" class="periode">
  <div class="periode">
    <label for="periode">Periode: </label>
    <br />
    <select name='periode'>
      <option value='0' selected>Tous les entraînements</option>
      <option value='7'>Juillet</option>
      <option value='8'>Aout</option>
      <option value='9'>Septembre</option>
      <option value='10'>Octobre</option>
      <option value='11'>Novembre</option>
      <option value='12'>Décembre</option>
      <option value='1'>Janvier</option>
      <option value='2'>Février</option>
      <option value='3'>Mars</option>
      <option value='4'>Avril</option>
      <option value='5'>Mai</option>
      <option value='6'>Juin</option>
    </select>
  </div>

  <input type='hidden' name='idMembre' value='<?php echo $idMembre; ?>'>

  <div class="ajoutMembre">
    <input type="submit" value="Valider">
  </div>
</form>

<div class = 'graphe' id="grapheCategorie" style="width: 900px; height: 500px;"></div>

<form action="../controleur/visualisationJoueur.php" method="POST">
  <select name="categorie">
    <?php
    // on selectionne tout les entraineurs disponible
    for ($i = 0; $i < count($listeCategories); $i++)
    {
      $idCategories = $listeCategories[$i]['idCategorie'];
      $libCategories = $listeCategories[$i]['libCategorie'];
      print "<option value=$idCategories>$libCategories</option>";
    }
    ?>
  </select>
  <?php
    if(isset($_POST["periode"]))
    {
      echo "<input type='hidden' name='periode' value='";
      echo $unMois;
      echo "'>";
    }
  ?>
  <input type='hidden' name='idMembre' value='<?php echo $idMembre; ?>'>
  <input type='submit' value='Plus de détails'>
</form>

<?php
  if (isset($_POST['categorie']))
  {
?>

    <br /><br />

    <div class = 'graphe' id="grapheSousCategorie" style="width: 900px; height: 500px;"></div>
    <form action="../controleur/visualisationJoueur.php" method="POST">
      <select name="souscategorie">
        <?php
        // on selectionne tout les entraineurs disponible
        for ($i = 0; $i < count($listeSousCategories); $i++)
        {
          $idSousCategories = $listeSousCategories[$i]['idSousCategorie'];
          $libSousCategories = $listeSousCategories[$i]['libSousCategorie'];
          print "<option value=$idSousCategories>$libSousCategories</option>";
        }
        ?>
      </select>
      <?php
        if(isset($_POST["periode"]))
        {
          echo "<input type='hidden' name='periode' value='";
          echo $unMois;
          echo "'>";
        }
      ?>
      <input type='hidden' name='idMembre' value='<?php echo $idMembre; ?>'>
      <input type='hidden' name='categorie' value='<?php echo $idCategorie; ?>'>
      <input type='submit' value='Plus de détails'>
    </form>
<?php
  }
?>

<?php
  if (isset($_POST['souscategorie']))
  {
?>

    <br /><br />

    <div class = 'graphe' id="grapheItem" style="width: 900px; height: 500px;"></div>

<?php
  }
?>
