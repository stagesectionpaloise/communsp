<link href="../css/visualisationPoids.css" rel="stylesheet" type="text/css">
<h1>Photo de <?= $unJoueur[0]['nom']; ?> <?= $unJoueur[0]['prenom']; ?>, <?= $uneDate; ?></h1>

<?php
  if($unLienPhoto == "")
  {
    echo "Aucune photo à afficher";
  }
  else
  {
?>
    <img class="fit-picture" src="../photoJoueurPoids/<?php echo $unLienPhoto ?>" alt="photoJoueur">
<?php
  }
?>


<form action='../controleur/visualisationPoids.php' method='post'>
  <input type='hidden' name='idMembre' value='<?php echo $idMembre ?>'>
  <input type='submit' value='Retour à la morpho'>
</form>

<?php
if ($_SESSION["statut"]<=1)
{
?>

<h2>Modification</h2>

<form action="../controleur/affichagePhotoMorpho.php" method="post" class="ajoutPoids" enctype="multipart/form-data">

  <div class="ajoutPoids">
    <label for="poids">Poids: </label>
    <br />
    <input type="text" name="poids" placeholder="en kg">
    <br />
  </div>

  <div class="ajoutPoids">
    <label for="masseGraisseuse">Masse graisseuse: </label>
    <br />
    <input type="text" name="masseGraisseuse" placeholder="facultatif">
    <br />
  </div>

  <div class="ajoutPoids">
    <label for="taille">Taille: </label>
    <br />
    <input type="text" name="taille" placeholder="facultatif">
    <br />
  </div>

  <div class="ajoutMembre">
    <label for="file">Photo poids (facultatif)</label>
    <br />
    <input type="file" name="file">
    <br />
  </div>

  <input type='hidden' name='idMembre' value='<?php echo $idMembre; ?>'>
  <input type='hidden' name='dateMorpho' value='<?php echo $uneDate; ?>'>
  <input type='hidden' name='lienPhoto' value='<?php echo $unLienPhoto; ?>'>
  <input type='hidden' name='modif' value='1'>
  <input type='hidden' name='token' value='<?php echo $_SESSION['token']; ?>'>

  <input type="submit" value="Valider">

</form>

<?php
}
?>
