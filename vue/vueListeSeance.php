<body>
<link rel="stylesheet" href="../css/styleListSeance.css">
<h1 id="titre">Liste seances</h1>

<br /><br />

<!-- menu deroulant pour selectionner la duree de la seance -->
<form action='listeSeance.php' method='post'>
  <select name='periodeSeance'>
    <option value='1' selected>Semaine derniere</option>
    <option value='0'>Semaine prochaine</option>
    <option value='2'>Mois dernier</option>
    <option value='3'>Toutes les seances</option>
  </select>
  <input type="submit" name="validation" value="Valider">
</form>

<br /><br />
<?php
  if ($periodeSeance == 3)
  {
    //boucle pour voir tous les seances
    echo "<table>";
    for ($i = 0; $i < count($listeTousSeance); $i++)
    {
      echo "<tr>";
      echo "<th class='test'>";
      print $listeTousSeance[$i]['dateSeance'];
      print " ";
      print $listeTousSeance[$i]['heureDebut'];
      print " ";
      print $listeTousSeance[$i]['heureFin'];
      ?>
      <form action='creationCreneau.php' method='post'>
        <input type="hidden" value="<?php echo $listeTousSeance[$i]['dateSeance'] ?>" name="Ladate"/>
        <input type="hidden" value="<?php echo $listeTousSeance[$i]['idSeance'] ?>" name="idSeance"/>
        <input type="hidden" value="<?php echo $listeTousSeance[$i]['heureDebut'] ?>" name="heureDebut"/>
        <input type="hidden" value="<?php echo $listeTousSeance[$i]['heureFin'] ?>" name="heureFin"/>
        <input type='hidden' name='token' value='<?php echo $_SESSION['token']; ?>'>
        <input type="submit" value="Continuer"/>
      </form>
      <?php
      echo "</th>";
      echo "</tr>";
    }
    echo "</table>";
  }

  if ($periodeSeance == 2)
  {
    //boucle pour voir tous les Seance du mois dernier
    echo "<table>";
    for ($i = 0; $i < count($listeSeanceMoisDernier); $i++)
    {
      echo "<tr>";
      echo "<th class='test'>";
      print $listeSeanceMoisDernier[$i]['dateSeance'];
      print " ";
      print $listeSeanceMoisDernier[$i]['heureDebut'];
      print " ";
      print $listeSeanceMoisDernier[$i]['heureFin'];
      ?>
      <form action='creationCreneau.php' method='post'>
        <input type="hidden" value="<?php echo $listeTousSeance[$i]['dateSeance'] ?>" name="Ladate"/>
        <input type="hidden" value="<?php echo $listeTousSeance[$i]['idSeance'] ?>" name="idSeance"/>
        <input type="hidden" value="<?php echo $listeTousSeance[$i]['heureDebut'] ?>" name="heureDebut"/>
        <input type="hidden" value="<?php echo $listeTousSeance[$i]['heureFin'] ?>" name="heureFin"/>
        <input type='hidden' name='token' value='<?php echo $_SESSION['token']; ?>'>
        <input type="submit" value="Continuer"/>
      </form>
      <?php
      echo "<br />";
      echo "</th>";
      echo "</tr>";
    }
    echo "</table>";
  }

  if ($periodeSeance == 1)
  {
    //boucle pour voir toutes les seances de la semaine derniere
    echo "<table>";
    for ($i = 0; $i < count($listeSeanceSemaineDerniere); $i++)
    {
      echo "<tr>";
      echo "<th class='test'>";
      print $listeSeanceSemaineDerniere[$i]['dateSeance'];
      print " ";
      print $listeSeanceSemaineDerniere[$i]['heureDebut'];
      print " ";
      print $listeSeanceSemaineDerniere[$i]['heureFin'];
      ?>
      <form action='creationCreneau.php' method='post'>
        <input type="hidden" value="<?php echo $listeTousSeance[$i]['dateSeance'] ?>" name="Ladate"/>
        <input type="hidden" value="<?php echo $listeTousSeance[$i]['idSeance'] ?>" name="idSeance"/>
        <input type="hidden" value="<?php echo $listeTousSeance[$i]['heureDebut'] ?>" name="heureDebut"/>
        <input type="hidden" value="<?php echo $listeTousSeance[$i]['heureFin'] ?>" name="heureFin"/>
        <input type='hidden' name='token' value='<?php echo $_SESSION['token']; ?>'>
        <input type="submit" value="Continuer"/>
      </form>
      <?php
      echo "<br />";
      echo "</th>";
      echo "</tr>";
    }
    echo "</table>";
  }

  if ($periodeSeance == 0)
  {
    //boucle pour voir toutes les seances de la semaine prochaine
    echo "<table>";
    for ($i = 0; $i < count($listeSeanceSemaineProchaine); $i++)
    {
      echo "<tr>";
      echo "<th class='test'>";
      print $listeSeanceSemaineProchaine[$i]['dateSeance'];
      print " ";
      print $listeSeanceSemaineProchaine[$i]['heureDebut'];
      print " ";
      print $listeSeanceSemaineProchaine[$i]['heureFin'];
      ?>
      <form action='creationCreneau.php' method='post'>
        <input type="hidden" value="<?php echo $listeTousSeance[$i]['dateSeance'] ?>" name="Ladate"/>
        <input type="hidden" value="<?php echo $listeTousSeance[$i]['idSeance'] ?>" name="idSeance"/>
        <input type="hidden" value="<?php echo $listeTousSeance[$i]['heureDebut'] ?>" name="heureDebut"/>
        <input type="hidden" value="<?php echo $listeTousSeance[$i]['heureFin'] ?>" name="heureFin"/>
        <input type='hidden' name='token' value='<?php echo $_SESSION['token']; ?>'>
        <input type="submit" value="Continuer"/>
      </form>
      <?php
      echo "<br />";
      echo "</th>";
      echo "</tr>";
    }
    echo "</table>";
  }
?>
