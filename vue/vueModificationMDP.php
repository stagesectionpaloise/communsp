<link href="../css/modifierInfoJoueur.css" rel="stylesheet" type="text/css">
<h1>Modification mot de passe</h1>

<form action="../controleur/modifierMDP.php" method="post" class="ajoutMembre">

  <div class="ajoutMembre">
    <label for="prenom">Ancien mot de passe: </label>
    <br />
    <input type="password" name="ancienMDP" required>
  </div>

  <br />

  <div class="ajoutMembre">
    <label for="nom">Nouveau mot de passe: </label>
    <br />
    <input type="password" name="newMDP" required>
  </div>

  <br />

  <div class="ajoutMembre">
    <label for="username">Confirmation nouveau mot de passe: </label>
    <br />
    <input type="password" name="conNewMDP" required>
  </div>

  <br />
  <input type='hidden' name='idMembre' value='<?php echo $idMembre ?>;'>
  <input type='hidden' name='token' value='<?php echo $_SESSION['token']; ?>'>
  <input type='submit' value='Valider'>
</form>
