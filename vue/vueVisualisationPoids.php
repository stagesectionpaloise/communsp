<link href="../css/visualisationPoids.css" rel="stylesheet" type="text/css">
<h1>Visualisation morpho de <?= $unJoueur[0]['nom']; ?> <?= $unJoueur[0]['prenom']; ?></h1>

<div class="graphe" id="graphePoidsDate" style="width: 900px; height: 500px;"></div>

<br /><br />

<table>
  <tr>
    <th>Date</th>
    <th>Taille</th>
    <th>Poids</th>
    <th>MG</th>
    <th>Lien photo</th>
  </tr>
  <?php
    for ($i = 0; $i < count($listePoids); $i++)
    {
      echo "<tr>";

        echo "<td>";
          echo $listePoids[$i]['dateMorpho'];
        echo "</td>";

        echo "<td>";
          echo $listePoids[$i]['taille'];
          echo " cm";
        echo "</td>";

        echo "<td>";
          echo $listePoids[$i]['poids'];
          echo " kg";
        echo "</td>";

        echo "<td>";
          echo $listePoids[$i]['masseGraisseuse'];
          if ($listePoids[$i]['masseGraisseuse'] != 0)
          {
            echo " kg";
          }
        echo "</td>";

        echo "<td>";
      ?>
        <form action='../controleur/affichagePhotoMorpho.php' method='post'>
          <input type='hidden' name='idMembre' value='<?php echo $listePoids[$i]['idMembre'] ?>'>
          <input type='hidden' name='dateMorpho' value='<?php echo $listePoids[$i]['dateMorpho'] ?>'>
          <input type='hidden' name='lienPhoto' value='<?php echo $listePoids[$i]['lienPhotoPoids'] ?>'>
          <input type='submit' value='Voir photo'>
        </form>
      <?php
        echo "</td>";

      echo "</tr>";
    }
  ?>
</table>

<br />

<h2>Ajout d'un poids</h2>

<br />

<div class = "formAjoutPoids">
  <form action="../controleur/visualisationPoids.php" method="post" class="ajoutPoids" enctype="multipart/form-data">

    <div class="ajoutPoids">
      <label for="date">Date: </label>
      <br />
      <input type="date" name="dateMorpho" required>
      <br />
    </div>

    <div class="ajoutPoids">
      <label for="poids">Poids: </label>
      <br />
      <input type="text" name="poids" placeholder="en kg" required>
      <br />
    </div>

    <div class="ajoutPoids">
      <label for="masseGraisseuse">Masse graisseuse: </label>
      <br />
      <input type="text" name="masseGraisseuse" placeholder="facultatif">
      <br />
    </div>

    <div class="ajoutPoids">
      <label for="taille">Taille: </label>
      <br />
      <input type="text" name="taille" placeholder="facultatif">
      <br />
    </div>

    <div class="ajoutMembre">
      <label for="file">Photo (facultatif)</label>
      <br />
      <input type="file" name="file">
      <br />
    </div>

    <input type='hidden' name='idMembre' value='<?php echo $idMembre; ?>'>
    <input type='hidden' name='token' value='<?php echo $_SESSION['token']; ?>'>
    <input type='hidden' name='ajout' value='1'>

    <input type="submit" value="Valider">

  </form>
</div>
