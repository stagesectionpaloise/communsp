<link rel="stylesheet" href="../css/styleConnexion.css">

<!-- page de connexion pour se connecter -->

<div class="mobile-screen">
  <div class="header">
    <h1>Connexion</h1>
  </div>

  <div class="logo"></div>


  <form id="login-form" action="../controleur/connexion.php" method="POST">
    <input type="text" name="username" placeholder="Nom d'utilisateur">
    <input type="password" name="password" placeholder="Mot de passe">
    <input type="submit" class="login-btn"/>
  </form>


</div>
