<link href="../css/modifierInfoJoueur.css" rel="stylesheet" type="text/css">
<h1>Modifier info membre</h1>

<!-- formulaire pour ajouter un membre -->

<form action="../controleur/modifierInfoJoueur.php" method="post" class="ajoutMembre" enctype="multipart/form-data">

  <div class="ajoutMembre">
    <label for="prenom">Prénom: </label>
    <br />
    <input type="text" name="prenom" id="prenom">
  </div>

  <div class="ajoutMembre">
    <label for="nom">Nom: </label>
    <br />
    <input type="text" name="nom" id="nom">
  </div>

  <div class="ajoutMembre">
    <label for="username">Nom utilisateur: </label>
    <br />
    <input type="text" name="username" id="username">
  </div>

  <div class="ajoutMembre">
    <label for="mail">Mail: </label>
    <br />
    <input type="email" name="mail" id="mail">
  </div>

  <div class="ajoutMembre">
    <label for="tel">Telephone: </label>
    <br />
    <input type="tel" name="tel" id="tel">
  </div>

  <div class="ajoutMembre">
    <label for="dateNaissance">Date de naissance: </label>
    <br />
    <input type="date" name="dateNaissance" id="dateNaissance">
  </div>

  <div class="ajoutMembre">
    <label for="pays">Pays: </label>
    <br />
    <input type="text" name="pays" id="pays">
  </div>

  <div class="ajoutMembre">
    <label for="scolaire">Scolaire: </label>
    <br />
    <input type="text" name="scolaire" id="scolaire">
  </div>

  <div class="ajoutMembre">
    <label for="dureeContrat">Durée de contrat: </label>
    <br />
    <input type="text" name="dureeContrat" id="dureeContrat">
  </div>

  <div class="ajoutMembre">
    <label for="pays">Poste: </label>
    <br />
    <select name='poste'>
      <option value='0'>------</option>
      <option value='1'>1-Pilier</option>
      <option value='2'>2-Talonneur</option>
      <option value='3'>3-Pilier</option>
      <option value='4'>4-Deuxième ligne</option>
      <option value='5'>5-Deuxiéme ligne</option>
      <option value='6'>6-Troisième ligne aile</option>
      <option value='7'>7-Troisième ligne aile</option>
      <option value='8'>8-Troisième ligne centre</option>
      <option value='9'>9-Demi de mêlée</option>
      <option value='10'>10-Demi d'ouverture</option>
      <option value='11'>11-Trois quarts aile</option>
      <option value='12'>12-Trois quarts centre</option>
      <option value='13'>13-Trois quarts centre</option>
      <option value='14'>14-Trois quarts aile</option>
      <option value='15'>15-Arrière</option>
    </select>
  </div>

  <div class="ajoutMembre">
    <label for="poste2">Poste 2: </label>
    <br />
    <select name='poste2'>
      <option value='0'>------</option>
      <option value='1'>1-Pilier</option>
      <option value='2'>2-Talonneur</option>
      <option value='3'>3-Pilier</option>
      <option value='4'>4-Deuxième ligne</option>
      <option value='5'>5-Deuxiéme ligne</option>
      <option value='6'>6-Troisième ligne aile</option>
      <option value='7'>7-Troisième ligne aile</option>
      <option value='8'>8-Troisième ligne centre</option>
      <option value='9'>9-Demi de mêlée</option>
      <option value='10'>10-Demi d'ouverture</option>
      <option value='11'>11-Trois quarts aile</option>
      <option value='12'>12-Trois quarts centre</option>
      <option value='13'>13-Trois quarts centre</option>
      <option value='14'>14-Trois quarts aile</option>
      <option value='15'>15-Arrière</option>
    </select>
  </div>

  <div class="ajoutMembre">
    <label for="JIFF">JIFF: </label>
    <br />
    <select name='JIFF'>
      <option value='0'>Non</option>
      <option value='1'>Oui</option>
    </select>
  </div>

  <div class="ajoutMembre">
    <label for="file">Photo:</label>
    <br />
    <input type="file" name="file">
  </div>

  <input type='hidden' name='idMembre' value='<?php echo $idMembre; ?>'>
  <input type='hidden' name='token' value='<?php echo $_SESSION['token']; ?>'>

  <div class="ajoutMembre">
    <input type="submit" value="Valider">
  </div>
</form>
