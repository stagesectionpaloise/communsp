
<link rel="stylesheet" href="../css/styleCreationCreneau.css">

<h1 class="titre">Creation de créneau(x)</h1>

<a>Renseigner la durée de l'entrainement</a>
<form action="../controleur/creationCreneau.php" method="POST">
  <input type="text" id="duree" name="duree" placeholder="en minutes" required>
  <br/>
  <br/>
  <h3 class="titreBis">Choisissez les entraineurs pour ce créneau</h3>
  <?php
  for ($i = 0; $i < count($entraineurs); $i++)
  {
    $idEntraineur = $entraineurs[$i]['idMembre'];
    $nomEntraineur = $entraineurs[$i]['nom'];
    $prenomEntraineur = $entraineurs[$i]['prenom'];
    ?>
    <div>
     <input type="checkbox" name="entraineur[]" class="container" id="entraineur[]" value="<?php echo $idEntraineur ?>" />
     <label for="entraineur[]" class="txtBox"><?php echo $nomEntraineur;echo " "; echo $prenomEntraineur ?></label>
   </div>
   <?php
  }
  ?><br /><br /><h3 class="titreBis">Cocher les joueurs présent</h3><br /><?php
  for($i = 0; $i < count($membres); $i++)
  {
    ?>
    <div>
      <input type="checkbox" name="joueur[]" class="container" id="joueur[]" value="<?php echo $membres[$i]['idMembre'] ?>" />
      <label for="joueur[]" class="txtBox"><?php echo $membres[$i]['nom'];echo " "; echo $membres[$i]['prenom']?></label>
      <br />
      <br />
    </div>
    <?php
  }
?>


   <br />
   <input type="hidden" value="<?php echo $DateEntr ?>" name="Ladate"/>
   <input type="hidden" value="<?php echo $idSeance ?>" name="idSeance"/>
   <input type="hidden" value="<?php echo $heureDebut ?>" name="heureDebut"/>
   <input type="hidden" value="<?php echo $heureFin ?>" name="heureFin"/>
   <input type='hidden' name='token' value='<?php echo $_SESSION['token']; ?>'>
   <input type="submit"/>
</form>

<h3 class="titreBis">Creneaux pour cette séance :</h3>

<?php

if (empty($libelItem))
{
  echo "pas de créneau sur cette séance <br /><br />";
}
else
{
  for ($i = 0; $i < count($libelItem); $i++)
    {
      echo "type d'entrainement : <br />categorie : ".$tabLibelCateg[$i]." <br /> sous-categorie : ".$tabLibelSousCateg[$i]."</br >Item : ".$libelItem[$i]. "<br />duree : ".$duree[$i];
      echo "<br />";
      echo '<form action="../controleur/creationCreneau.php" method="POST">';
      echo "<input type=submit name=DeleteCreneau value=supprimer />";
      ?>
      <input type="hidden" value="<?php echo $DateEntr ?>" name="Ladate"/>
      <input type="hidden" value="<?php echo $idSeance ?>" name="idSeance"/>
      <input type="hidden" value="<?php echo $idCre[$i] ?>" name="idCreneau"/>
      <input type="hidden" value="<?php echo $heureDebut ?>" name="heureDebut"/>
      <input type="hidden" value="<?php echo $heureFin ?>" name="heureFin"/>
      <input type='hidden' name='token' value='<?php echo $_SESSION['token']; ?>'>
      <?php
      echo "</form>";
      echo "<br />";
      ?>
      <form action="detailCreneau.php" method=post>
        <input type=hidden value="<?php echo $idCre[$i] ?>" name=idCreneau />
        <input type=hidden value="<?php echo $tabLibelCateg[$i] ?>" name=tabLibelCateg />
        <input type=hidden value="<?php echo $tabLibelSousCateg[$i] ?>" name=tabLibelSousCateg />
        <input type=hidden value="<?php echo $libelItem[$i] ?>" name=libelItem />
        <input type=hidden value="<?php echo $duree[$i] ?>" name=duree />
        <input type="hidden" value="<?php echo $DateEntr ?>" name="Ladate"/>
        <input type="hidden" value="<?php echo $idSeance ?>" name="idSeance"/>
        <input type="hidden" value="<?php echo $heureDebut ?>" name="heureDebut"/>
        <input type="hidden" value="<?php echo $heureFin ?>" name="heureFin"/>
        <input type="hidden" value="<?php echo $idCre[$i]?>" name="idCreneau"/>
        <input type='hidden' name='token' value='<?php echo $_SESSION['token']; ?>'>
        <input type="submit" value="Detail du creneau" />
      </form>
      <?php
    }
}
?>
