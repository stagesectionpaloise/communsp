<link href="../css/ajoutMembre.css" rel="stylesheet" type="text/css">
<h1>Ajout membre</h1>

<!-- formulaire pour ajouter un membre -->
<form action="../controleur/inscription.php" method="post" class="ajoutMembre">

  <div class="ajoutMembre">
    <label for="prenom">Prénom: </label>
    <br />
    <input type="text" name="prenom" id="prenom" required>
  </div>

  <div class="ajoutMembre">
    <label for="nom">Nom: </label>
    <br />
    <input type="text" name="nom" id="nom" required>
  </div>

  <div class="ajoutMembre">
    <label for="username">Nom utilisateur: </label>
    <br />
    <input type="text" name="username" id="username" required>
  </div>

  <div class="ajoutMembre">
    <label for="mail">Mail: </label>
    <br />
    <input type="email" name="mail" id="mail" required>
  </div>

  <div class="ajoutMembre">
    <label for="tel">Telephone: </label>
    <br />
    <input type="tel" name="tel" id="tel" required>
  </div>

  <div class="ajoutMembre">
    <label for="password">Mot de passe: </label>
    <br />
    <input type="password" name="password" id="password" required>
  </div>

  <div class="ajoutMembre">
    <label for="passwordConfirm">Confirmer mot de passe: </label>
    <br />
    <input type="password" name="passwordConfirm" id="passwordConfirm" required>
  </div>

  <div class="ajoutMembre">
    <label for="statut">Statut: </label>
    <br />
    <select name='statut'>
      <option value='2' selected>Joueur</option>
      <option value='1'>Entraîneur</option>
    </select>
  </div>

  <input type='hidden' name='token' value='<?php echo $_SESSION['token']; ?>'>

  <div class="ajoutMembre">
    <input type="submit" value="Valider">
  </div>
</form>
