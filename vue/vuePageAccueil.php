<link href="../css/pageAccueil.css" rel="stylesheet" type="text/css">
<h1>Page d'accueil</h1>

<br /><br />

<!-- <table>
    <thead>
        <tr>
            <th><h2>Joueurs aptes : <?php echo $nbJoueurApte[0]['nbJoueur'] ?></h2></th>
            <th><h2>Joueurs pro : <?php echo $nbJoueurPro[0]['nbJoueur'] ?></h2></th>
        </tr>
    </thead>
    <tbody>
        <tr>

            <td>
              <table>
                <?php
                  for ($i = 0; $i < count($listeJoueurApte); $i++)
                  {

                    //affichage des infos joueur
                    echo "<tr>";
                    echo "<td>";
                    echo $listeJoueurApte[$i]['nom'];
                    echo " </td>";
                    echo "<td>";
                    echo $listeJoueurApte[$i]['prenom'];
                    echo "</td>";
                    echo "<td>";
                    echo $listeJoueurApte[$i]['tel'];
                    echo "</td>";
                    echo "<td>";
                    echo $listeJoueurApte[$i]['mail'];
                    echo "</td>";
                    echo "<td>";
                    echo "
                    <form action='updateSituationJoueur.php' method='post'>
                      <input type='hidden' name='idMembre' value='";
                    echo $listeJoueurApte[$i]['idMembre'];
                    echo "'/>
                        <select name='idSituation'>
                          <option value='1' selected>Apte</option>
                          <option value='2'>Pro</option>
                          <option value='3'>Blessé</option>
                          <option value='4'>École</option>
                          <option value='5'>Autre</option>
                        </select>
                        <input type='submit' value='->'>
                      </form>
                    ";
                    echo "</td>";
                    echo "<td>";
                    echo "
                      <form action='../controleur/visualisationJoueur.php' method='post'>
                        <input type='hidden' name='idMembre' value='";
                    echo $listeJoueurApte[$i]['idMembre'];
                    echo "'/>
                        <input type='submit' value='plus de détails'>
                      </form>
                    ";
                    echo "</td>";
                    echo "</tr>";
                  }

                ?>
              </table>
            </td>

            <td>
              <table>
                <?php
                  for ($i = 0; $i < count($listeJoueurPro); $i++)
                  {

                    //affichage des infos joueur
                    echo "<tr>";
                    echo "<td>";
                    echo $listeJoueurPro[$i]['nom'];
                    echo " </td>";
                    echo "<td>";
                    echo $listeJoueurPro[$i]['prenom'];
                    echo "</td>";
                    echo "<td>";
                    echo $listeJoueurPro[$i]['tel'];
                    echo "</td>";
                    echo "<td>";
                    echo $listeJoueurPro[$i]['mail'];
                    echo "</td>";
                    echo "<td>";
                    echo "
                    <form action='updateSituationJoueur.php' method='post'>
                      <input type='hidden' name='idMembre' value='";
                    echo $listeJoueurPro[$i]['idMembre'];
                    echo "'/>
                        <select name='idSituation'>
                          <option value='1'>Apte</option>
                          <option value='2' selected>Pro</option>
                          <option value='3'>Blessé</option>
                          <option value='4'>École</option>
                          <option value='5'>Autre</option>
                        </select>
                        <input type='submit' value='->'>
                      </form>
                    ";
                    echo "</td>";
                    echo "<td>";
                    echo "
                      <form action='../controleur/visualisationJoueur.php' method='post'>
                        <input type='hidden' name='idMembre' value='";
                    echo $listeJoueurPro[$i]['idMembre'];
                    echo "'/>
                        <input type='submit' value='plus de détails'>
                      </form>
                    ";
                    echo "</td>";
                    echo "</tr>";
                  }

                ?>
              </table>
            </td>

        </tr>
    </tbody>
    <thead>
        <tr>
            <th><h2>Joueurs blessés : <?php echo $nbJoueurBlesse[0]['nbJoueur'] ?></h2></th>
            <th><h2>Joueurs école : <?php echo $nbJoueurEcole[0]['nbJoueur'] ?></h2></th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>
              <table>
                <?php
                  for ($i = 0; $i < count($listeJoueurBlesse); $i++)
                  {

                    //affichage des infos joueur
                    echo "<tr>";
                    echo "<td>";
                    echo $listeJoueurBlesse[$i]['nom'];
                    echo " </td>";
                    echo "<td>";
                    echo $listeJoueurBlesse[$i]['prenom'];
                    echo "</td>";
                    echo "<td>";
                    echo $listeJoueurBlesse[$i]['tel'];
                    echo "</td>";
                    echo "<td>";
                    echo $listeJoueurBlesse[$i]['mail'];
                    echo "</td>";
                    echo "<td>";
                    echo "
                    <form action='updateSituationJoueur.php' method='post'>
                      <input type='hidden' name='idMembre' value='";
                    echo $listeJoueurBlesse[$i]['idMembre'];
                    echo "'/>
                        <select name='idSituation'>
                          <option value='1'>Apte</option>
                          <option value='2'>Pro</option>
                          <option value='3' selected>Blessé</option>
                          <option value='4'>École</option>
                          <option value='5'>Autre</option>
                        </select>
                        <input type='submit' value='->'>
                      </form>
                    ";
                    echo "</td>";
                    echo "<td>";
                    echo "
                      <form action='../controleur/visualisationJoueur.php' method='post'>
                        <input type='hidden' name='idMembre' value='";
                    echo $listeJoueurBlesse[$i]['idMembre'];
                    echo "'/>
                        <input type='submit' value='plus de détails'>
                      </form>
                    ";
                    echo "</td>";
                    echo "</tr>";
                  }

                ?>
              </table>
            </td>

            <td>
              <table>
                <?php
                  for ($i = 0; $i < count($listeJoueurEcole); $i++)
                  {

                    //affichage des infos joueur
                    echo "<tr>";
                    echo "<td>";
                    echo $listeJoueurEcole[$i]['nom'];
                    echo " </td>";
                    echo "<td>";
                    echo $listeJoueurEcole[$i]['prenom'];
                    echo "</td>";
                    echo "<td>";
                    echo $listeJoueurEcole[$i]['tel'];
                    echo "</td>";
                    echo "<td>";
                    echo $listeJoueurEcole[$i]['mail'];
                    echo "</td>";
                    echo "<td>";
                    echo "
                    <form action='updateSituationJoueur.php' method='post'>
                      <input type='hidden' name='idMembre' value='";
                    echo $listeJoueurEcole[$i]['idMembre'];
                    echo "'/>
                        <select name='idSituation'>
                          <option value='1'>Apte</option>
                          <option value='2'>Pro</option>
                          <option value='3'>Blessé</option>
                          <option value='4' selected>École</option>
                          <option value='5'>Autre</option>
                        </select>
                        <input type='submit' value='->'>
                      </form>
                    ";
                    echo "</td>";
                    echo "<td>";
                    echo "
                      <form action='../controleur/visualisationJoueur.php' method='post'>
                        <input type='hidden' name='idMembre' value='";
                    echo $listeJoueurEcole[$i]['idMembre'];
                    echo "'/>
                        <input type='submit' value='plus de détails'>
                      </form>
                    ";
                    echo "</td>";
                    echo "</tr>";
                  }

                ?>
              </table>
            </td>
        </tr>
    </tbody>
    <thead>
        <tr>
            <th><h2>Joueurs autre : <?php echo $nbJoueurAutre[0]['nbJoueur'] ?></h2></th>
            <th></th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>
              <table>
                <?php
                  for ($i = 0; $i < count($listeJoueurAutre); $i++)
                  {

                    //affichage des infos joueur
                    echo "<tr>";
                    echo "<td>";
                    echo $listeJoueurAutre[$i]['nom'];
                    echo " </td>";
                    echo "<td>";
                    echo $listeJoueurAutre[$i]['prenom'];
                    echo "</td>";
                    echo "<td>";
                    echo $listeJoueurAutre[$i]['tel'];
                    echo "</td>";
                    echo "<td>";
                    echo $listeJoueurAutre[$i]['mail'];
                    echo "</td>";
                    echo "<td>";
                    echo "
                    <form action='updateSituationJoueur.php' method='post'>
                      <input type='hidden' name='idMembre' value='";
                    echo $listeJoueurAutre[$i]['idMembre'];
                    echo "'/>
                        <select name='idSituation'>
                          <option value='1'>Apte</option>
                          <option value='2'>Pro</option>
                          <option value='3'>Blessé</option>
                          <option value='4'>École</option>
                          <option value='5' selected>Autre</option>
                        </select>
                        <input type='submit' value='->'>
                      </form>
                    ";
                    echo "</td>";
                    echo "<td>";
                    echo "
                      <form action='../controleur/visualisationJoueur.php' method='post'>
                        <input type='hidden' name='idMembre' value='";
                    echo $listeJoueurAutre[$i]['idMembre'];
                    echo "'/>
                        <input type='submit' value='plus de détails'>
                      </form>
                    ";
                    echo "</td>";
                    echo "</tr>";
                  }

                ?>
              </table>
            </td>

            <td>

            </td>
        </tr>
    </tbody>
</table> -->

<h2>Joueurs aptes : <?php echo $nbJoueurApte[0]['nbJoueur'] ?></h2>

<br />

<table>
<?php
  for ($i = 0; $i < count($listeJoueurApte); $i++)
  {

    //affichage des infos joueur
    echo "<tr>";
    echo "<td>";
    echo $listeJoueurApte[$i]['nom'];
    echo " </td>";
    echo "<td>";
    echo $listeJoueurApte[$i]['prenom'];
    echo "</td>";
    echo "<td>";
    echo $listeJoueurApte[$i]['tel'];
    echo "</td>";
    echo "<td>";
    echo $listeJoueurApte[$i]['mail'];
    echo "</td>";
    echo "<td>";
    echo "
    <form action='updateSituationJoueur.php' method='post'>
      <input type='hidden' name='idMembre' value='";
    echo $listeJoueurApte[$i]['idMembre'];
    echo "'/>
        <select name='idSituation'>
          <option value='1' selected>Apte</option>
          <option value='2'>Pro</option>
          <option value='3'>Blessé</option>
          <option value='4'>École</option>
          <option value='5'>Autre</option>
        </select>
        <input type='submit' value='->'>
      </form>
    ";
    echo "</td>";
    echo "<td>";
    echo "
      <form action='../controleur/visualisationJoueur.php' method='post'>
        <input type='hidden' name='idMembre' value='";
    echo $listeJoueurApte[$i]['idMembre'];
    echo "'/>
        <input type='submit' value='plus de détails'>
      </form>
    ";
    echo "</td>";
    echo "</tr>";
  }

?>
</table>

<br /><br />

<h2>Joueurs pro : <?php echo $nbJoueurPro[0]['nbJoueur'] ?></h2>

<br />

<table>
<?php
  for ($i = 0; $i < count($listeJoueurPro); $i++)
  {

    //affichage des infos joueur
    echo "<tr>";
    echo "<td>";
    echo $listeJoueurPro[$i]['nom'];
    echo " </td>";
    echo "<td>";
    echo $listeJoueurPro[$i]['prenom'];
    echo "</td>";
    echo "<td>";
    echo $listeJoueurPro[$i]['tel'];
    echo "</td>";
    echo "<td>";
    echo $listeJoueurPro[$i]['mail'];
    echo "</td>";
    echo "<td>";
    echo "
    <form action='updateSituationJoueur.php' method='post'>
      <input type='hidden' name='idMembre' value='";
    echo $listeJoueurPro[$i]['idMembre'];
    echo "'/>
        <select name='idSituation'>
          <option value='1'>Apte</option>
          <option value='2' selected>Pro</option>
          <option value='3'>Blessé</option>
          <option value='4'>École</option>
          <option value='5'>Autre</option>
        </select>
        <input type='submit' value='->'>
      </form>
    ";
    echo "</td>";
    echo "<td>";
    echo "
      <form action='../controleur/visualisationJoueur.php' method='post'>
        <input type='hidden' name='idMembre' value='";
    echo $listeJoueurPro[$i]['idMembre'];
    echo "'/>
        <input type='submit' value='plus de détails'>
      </form>
    ";
    echo "</td>";
    echo "</tr>";
  }

?>
</table>

<br /><br />

<h2>Joueurs blessés : <?php echo $nbJoueurBlesse[0]['nbJoueur'] ?></h2>

<br />

<table>
<?php
  for ($i = 0; $i < count($listeJoueurBlesse); $i++)
  {

    //affichage des infos joueur
    echo "<tr>";
    echo "<td>";
    echo $listeJoueurBlesse[$i]['nom'];
    echo " </td>";
    echo "<td>";
    echo $listeJoueurBlesse[$i]['prenom'];
    echo "</td>";
    echo "<td>";
    echo $listeJoueurBlesse[$i]['tel'];
    echo "</td>";
    echo "<td>";
    echo $listeJoueurBlesse[$i]['mail'];
    echo "</td>";
    echo "<td>";
    echo "
    <form action='updateSituationJoueur.php' method='post'>
      <input type='hidden' name='idMembre' value='";
    echo $listeJoueurBlesse[$i]['idMembre'];
    echo "'/>
        <select name='idSituation'>
          <option value='1'>Apte</option>
          <option value='2'>Pro</option>
          <option value='3' selected>Blessé</option>
          <option value='4'>École</option>
          <option value='5'>Autre</option>
        </select>
        <input type='submit' value='->'>
      </form>
    ";
    echo "</td>";
    echo "<td>";
    echo "
      <form action='../controleur/visualisationJoueur.php' method='post'>
        <input type='hidden' name='idMembre' value='";
    echo $listeJoueurBlesse[$i]['idMembre'];
    echo "'/>
        <input type='submit' value='plus de détails'>
      </form>
    ";
    echo "</td>";
    echo "</tr>";
  }

?>
</table>

<br /><br />

<h2>Joueurs école : <?php echo $nbJoueurEcole[0]['nbJoueur'] ?></h2>

<br />

<table>
<?php
  for ($i = 0; $i < count($listeJoueurEcole); $i++)
  {

    //affichage des infos joueur
    echo "<tr>";
    echo "<td>";
    echo $listeJoueurEcole[$i]['nom'];
    echo " </td>";
    echo "<td>";
    echo $listeJoueurEcole[$i]['prenom'];
    echo "</td>";
    echo "<td>";
    echo $listeJoueurEcole[$i]['tel'];
    echo "</td>";
    echo "<td>";
    echo $listeJoueurEcole[$i]['mail'];
    echo "</td>";
    echo "<td>";
    echo "
    <form action='updateSituationJoueur.php' method='post'>
      <input type='hidden' name='idMembre' value='";
    echo $listeJoueurEcole[$i]['idMembre'];
    echo "'/>
        <select name='idSituation'>
          <option value='1'>Apte</option>
          <option value='2'>Pro</option>
          <option value='3'>Blessé</option>
          <option value='4' selected>École</option>
          <option value='5'>Autre</option>
        </select>
        <input type='submit' value='->'>
      </form>
    ";
    echo "</td>";
    echo "<td>";
    echo "
      <form action='../controleur/visualisationJoueur.php' method='post'>
        <input type='hidden' name='idMembre' value='";
    echo $listeJoueurEcole[$i]['idMembre'];
    echo "'/>
        <input type='submit' value='plus de détails'>
      </form>
    ";
    echo "</td>";
    echo "</tr>";
  }

?>
</table>

<br /><br />

<h2>Joueurs autre : <?php echo $nbJoueurAutre[0]['nbJoueur'] ?></h2>

<br />

<table>
<?php
  for ($i = 0; $i < count($listeJoueurAutre); $i++)
  {

    //affichage des infos joueur
    echo "<tr>";
    echo "<td>";
    echo $listeJoueurAutre[$i]['nom'];
    echo " </td>";
    echo "<td>";
    echo $listeJoueurAutre[$i]['prenom'];
    echo "</td>";
    echo "<td>";
    echo $listeJoueurAutre[$i]['tel'];
    echo "</td>";
    echo "<td>";
    echo $listeJoueurAutre[$i]['mail'];
    echo "</td>";
    echo "<td>";
    echo "
    <form action='updateSituationJoueur.php' method='post'>
      <input type='hidden' name='idMembre' value='";
    echo $listeJoueurAutre[$i]['idMembre'];
    echo "'/>
        <select name='idSituation'>
          <option value='1'>Apte</option>
          <option value='2'>Pro</option>
          <option value='3'>Blessé</option>
          <option value='4'>École</option>
          <option value='5' selected>Autre</option>
        </select>
        <input type='submit' value='->'>
      </form>
    ";
    echo "</td>";
    echo "<td>";
    echo "
      <form action='../controleur/visualisationJoueur.php' method='post'>
        <input type='hidden' name='idMembre' value='";
    echo $listeJoueurAutre[$i]['idMembre'];
    echo "'/>
        <input type='submit' value='plus de détails'>
      </form>
    ";
    echo "</td>";
    echo "</tr>";
  }

?>
</table>
