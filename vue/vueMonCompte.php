<link href="../css/visualisationJoueur.css" rel="stylesheet" type="text/css">
<h1>Mon compte</h1>

<table>
  <tr>
   <td>Nom</td>
   <td><?= $unJoueur[0]['nom']; ?></td>
  </tr>

  <tr>
   <td>Prenom</td>
   <td><?= $unJoueur[0]['prenom']; ?></td>
  </tr>

  <tr>
   <td>Nom d'utilisateur</td>
   <td><?= $unJoueur[0]['username']; ?></td>
  </tr>

  <tr>
   <td>Numéro de téléphone</td>
   <td><?= $unJoueur[0]['tel']; ?></td>
  </tr>

  <tr>
   <td>Adresse mail</td>
   <td><?= $unJoueur[0]['mail']; ?></td>
  </tr>

  <tr>
   <td>Date de naissance</td>
   <td><?= $unJoueur[0]['dateNaissance']; ?></td>
  </tr>
</table>

<br />

<form action='../controleur/modificationInfoCompte.php' method='post'>
  <input type='hidden' name='idMembre' value='<?php echo $idMembre ?>;'>
  <input class= 'boutonMorpho' type='submit' value='Modifier info compte'>
</form>

<form action='../controleur/modificationMdp.php' method='post'>
  <input type='hidden' name='idMembre' value='<?php echo $idMembre ?>;'>
  <input class= 'boutonMorpho' type='submit' value='Modifier mot de passe'>
</form>
