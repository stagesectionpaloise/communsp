<link href="../css/modifierInfoJoueur.css" rel="stylesheet" type="text/css">
<h1>Modifier info compte</h1>

<form action="../controleur/modifierInfoCompte.php" method="post" class="ajoutMembre">

  <div class="ajoutMembre">
    <label for="prenom">Prénom: </label>
    <br />
    <input type="text" name="prenom" id="prenom">
  </div>

  <br />

  <div class="ajoutMembre">
    <label for="nom">Nom: </label>
    <br />
    <input type="text" name="nom" id="nom">
  </div>

  <br />

  <div class="ajoutMembre">
    <label for="username">Nom utilisateur: </label>
    <br />
    <input type="text" name="username" id="username">
  </div>

  <br />

  <div class="ajoutMembre">
    <label for="mail">Mail: </label>
    <br />
    <input type="email" name="mail" id="mail">
  </div>

  <br />

  <div class="ajoutMembre">
    <label for="tel">Telephone: </label>
    <br />
    <input type="tel" name="tel" id="tel">
  </div>

  <br />

  <div class="ajoutMembre">
    <label for="dateNaissance">Date de naissance: </label>
    <br />
    <input type="date" name="dateNaissance" id="dateNaissance">
  </div>

  <br />
  <input type='hidden' name='idMembre' value='<?php echo $idMembre ?>;'>
  <input type='hidden' name='token' value='<?php echo $_SESSION['token']; ?>'>
  <input type='submit' value='Valider'>
</form>
