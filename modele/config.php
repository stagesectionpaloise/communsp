<?php

  //Fichier de connexion à la base de données

  function connexionPDO()
  {
    //Base de données en ligne

    // $login = "EXE_12_ake84";
    // $mdp = "C1[Mg](ZWMbB1bND";
    // $bd = "EXE_12_ake84";
    // $serveur = "exedesk.fr";

    //Base de données local

    $login = "root";
    $mdp = "";
    $bd = "cdfsectionpaloise2";
    $serveur = "localhost";

    try
    {
        $conn = new PDO("mysql:host=$serveur;dbname=$bd", $login, $mdp, array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES \'UTF8\''));
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        return $conn;
    }

    catch (PDOException $e)
    {
        print "Erreur de connexion PDO ";
        die();
    }
  }

 ?>
