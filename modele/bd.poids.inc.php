<?php
//appel de la page config pour se connecter à la base de données
include_once "config.php";
include_once "bd.joueur.inc.php";

function getPoidsByIdMembre($idMembre)
{
    $resultat = array();

    try
    {
        $cnx = connexionPDO();
        $req = $cnx->prepare("select * from membre, morpho where membre.idMembre=morpho.idMembre and morpho.idMembre=:idMembre");
        $req->bindValue(':idMembre', $idMembre, PDO::PARAM_INT);

        $req->execute();

        $resultat = $req->fetchAll(PDO::FETCH_ASSOC);
    }
    catch (PDOException $e)
    {
        print "Erreur !: " . $e->getMessage();
        die();
    }
    return $resultat;
}

function addPoids($dateMorpho, $idMembre, $poids, $taille, $masseGraisseuse, $lienPhotoPoids)
{
    try
    {
        $cnx = connexionPDO();
        $req = $cnx->prepare("insert into morpho (dateMorpho, idMembre, poids, taille, masseGraisseuse, lienPhotoPoids) values(:dateMorpho, :idMembre, :poids, :taille, :masseGraisseuse, :lienPhotoPoids)");
        $req->bindValue(':dateMorpho', $dateMorpho, PDO::PARAM_STR);
        $req->bindValue(':idMembre', $idMembre, PDO::PARAM_INT);
        $req->bindValue(':poids',$poids, PDO::PARAM_INT);
        $req->bindValue(':taille',$taille, PDO::PARAM_INT);
        $req->bindValue(':masseGraisseuse',$masseGraisseuse, PDO::PARAM_INT);
        $req->bindValue(':lienPhotoPoids',$lienPhotoPoids, PDO::PARAM_STR);

        $resultat = $req->execute();
    }
    catch (PDOException $e)
    {
        print "Erreur !: " . $e->getMessage();
        die();
    }
    return $resultat;
}

function updatePoids($dateMorpho, $idMembre, $poids, $taille, $masseGraisseuse, $name)
{

    try
    {
        $cnx = connexionPDO();

        if ($poids!="")
        {
          $req = $cnx->prepare("update morpho set poids = :poids where idMembre = :idMembre and dateMorpho = :dateMorpho");
          $req->bindValue(':poids', $poids, PDO::PARAM_INT);
          $req->bindValue(':dateMorpho', $dateMorpho, PDO::PARAM_STR);
          $req->bindValue(':idMembre', $idMembre, PDO::PARAM_INT);
          $req->execute();
        }

        if ($taille!="")
        {
          $req = $cnx->prepare("update morpho set taille = :taille where idMembre = :idMembre and dateMorpho = :dateMorpho");
          $req->bindValue(':taille', $taille, PDO::PARAM_INT);
          $req->bindValue(':dateMorpho', $dateMorpho, PDO::PARAM_STR);
          $req->bindValue(':idMembre', $idMembre, PDO::PARAM_INT);
          $req->execute();
        }

        if ($masseGraisseuse!="")
        {
          $req = $cnx->prepare("update morpho set masseGraisseuse = :masseGraisseuse where idMembre = :idMembre and dateMorpho = :dateMorpho");
          $req->bindValue(':masseGraisseuse', $masseGraisseuse, PDO::PARAM_INT);
          $req->bindValue(':dateMorpho', $dateMorpho, PDO::PARAM_STR);
          $req->bindValue(':idMembre', $idMembre, PDO::PARAM_INT);
          $req->execute();
        }

        if ($name!=NULL)
        {
          $req = $cnx->prepare("update morpho set lienPhotoPoids = :lienPhotoPoids where idMembre = :idMembre and dateMorpho = :dateMorpho");
          $req->bindValue(':lienPhotoPoids', $name, PDO::PARAM_STR);
          $req->bindValue(':dateMorpho', $dateMorpho, PDO::PARAM_STR);
          $req->bindValue(':idMembre', $idMembre, PDO::PARAM_INT);
          $req->execute();
        }

    }
    catch (PDOException $e)
    {
        print "Erreur !: " . $e->getMessage();
        die();
    }
}

?>
