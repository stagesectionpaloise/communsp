<?php
include_once "config.php";

// Fonction pour ajouter un entrainement sur la journée
function addEntrainement($laDate) {
    try {
        $cnx = connexionPDO();
        $req = $cnx->prepare("insert into entrainement (dateEntr) values(:laDate)");
        $req->bindValue(':laDate', $laDate, PDO::PARAM_STR);
        $resultat = $req->execute();
    } catch (PDOException $e) {
        print "Erreur !: " . $e->getMessage();
        die();
    }
    return $resultat;
}

// Fonction pour récupérer toutes les dates de la table entrainement
function getDateEntr() {
  $resultat = array();

  try
  {
    $cnx = connexionPDO();
    $req = $cnx->prepare("select dateEntr from entrainement");
    $req->execute();

    $ligne = $req->fetch(PDO::FETCH_ASSOC);
    while ($ligne)
    {
      $resultat[] = $ligne;
      $ligne = $req->fetch(PDO::FETCH_ASSOC);
    }
  }
  catch (PDOException $e) {
    print "Erreur !: " . $e->getMessage();
    die();
  }
  return $resultat;
}


function getEntrByDateEntr($DateEntr) {
  try {
    $cnx = connexionPDO();
    $req = $cnx->prepare("select idEntr from entrainement where dateEntr=:DateEntr");
    $req->bindValue(':DateEntr', $DateEntr, PDO::PARAM_STR);
    $req->execute();
    $resultat = $req->fetch(PDO::FETCH_ASSOC);
  }
  catch (PDOExcepetion $e) {
    print "Erreur !: " . $e->getMessage();
    die();
  }
  return $resultat;
}

// Fonction pour ajouter un creneau sur l'entrainement de la journée
function addCreneau($idEntr, $idItem, $heureDebut, $heureFin)
{
  try
  {
    $cnx = connexionPDO();
    $req = $cnx->prepare("insert into creneaux (idEntr, idItem, heureDebut, heureFin) values(:idEntr, :idItem, :heureDebut, :heureFin)");
    $req->bindValue(':idEntr', $idEntr, PDO::PARAM_INT);
    $req->bindValue(':idItem', $idItem, PDO::PARAM_INT);
    $req->bindValue(':heureDebut', $heureDebut, PDO::PARAM_STR);
    $req->bindValue(':heureFin', $heureFin, PDO::PARAM_STR);

    $resultat = $req->execute();
  }
  catch (PDOException $e)
  {
    print "Erreur !: " . $e->getMessage();
    die();
  }
  return $resultat;
}

function addParticiper($idCreneaux, $idMembre)
{
  try
  {
    $cnx = connexionPDO();
    $req = $cnx->prepare("insert into faire (idCreneaux, idMembre) values(:idCreneaux, :idMembre)");
    $req->bindValue(':idCreneaux', $idCreneaux, PDO::PARAM_INT);
    $req->bindValue(':idMembre', $idMembre, PDO::PARAM_INT);

    $resultat = $req->execute();
  }
  catch (PDOException $e)
  {
    print "Erreur !: " . $e->getMessage();
    die();
  }
  return $resultat;
}

function getEntraineurByStatus()
{
  try
  {
    $cnx = connexionPDO();
    $req = $cnx->prepare("select * from membre where statut=1");
    $req->execute();
    $resultat=$req->fetchAll(PDO::FETCH_ASSOC);
  }
  catch (PDOException $e)
  {
    print "Erreur !: " . $e->getMessage();
    die();
  }
  return $resultat;
}

function getItem() {
  try {
    $cnx = connexionPDO();
    $req = $cnx->prepare("select * from item");
    $req->execute();
    $resultat=$req->fetchAll(PDO::FETCH_ASSOC);
  }
  catch (PDOException $e) {
    print "Erreur !: " ; $e->getMessage();
    die();
  }
  return $resultat;
}

function getCreneau($idEntr) {
  try {
    $cnx = connexionPDO();
    $req = $cnx->prepare("select * from creneaux,item where creneaux.idItem = item.idItem and idEntr=:idEntr");
    $req->bindValue(':idEntr', $idEntr, PDO::PARAM_INT);
    $req->execute();
    $resultat=$req->fetchAll(PDO::FETCH_ASSOC);
  }
  catch (PDOException $e) {
    print "Erreur !: " . $e->getMessage();
    die();
  }
  return $resultat;
}

function getTousCreneaux()
{
  try
  {
    $cnx = connexionPDO();
    $req = $cnx->prepare("select * from creneaux");
    $req->execute();
    $resultat=$req->fetchAll(PDO::FETCH_ASSOC);
  }
  catch (PDOException $e)
  {
    print "Erreur !: " . $e->getMessage();
    die();
  }
  return $resultat;
}

function getIdEntrByIdCreneau($idCreneaux)
{
  try
  {
    $cnx = connexionPDO();
    $req = $cnx->prepare("select * from creneaux, faire, membre where creneaux.idCreneaux=faire.idCreneaux and faire.idMembre=membre.idMembre and creneaux.idCreneaux=:idCreneaux");
    $req->bindValue(':idCreneaux', $idCreneaux, PDO::PARAM_INT);
    $req->execute();
    $resultat=$req->fetchAll(PDO::FETCH_ASSOC);
  }
  catch (PDOException $e)
  {
    print "Erreur !: " . $e->getMessage();
    die();
  }
  return $resultat;
}

?>
