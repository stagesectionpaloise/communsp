<?php
include_once "config.php";

// fonction pour aller chercher les informations liée à l'utilisateur dans la base donnée
function getUtilisateurByUtilisateur($username) {
    $resultat = array();

    try {
        $cnx = connexionPDO();
        $req = $cnx->prepare("select * from membre where username=:username");
        $req->bindValue(':username', $username, PDO::PARAM_STR);
        $req->execute();

        $resultat = $req->fetch(PDO::FETCH_ASSOC);
    } catch (PDOException $e) {
        print "Erreur !: " . $e->getMessage();
        die();
    }
    return $resultat;
}

// fonction pour ajouter un utilisateur dans la base donnée
function addUtilisateur($prenom,$nom,$username,$password, $statut) {
    try {
        $cnx = connexionPDO();
        $req = $cnx->prepare("insert into membre (prenom, nom, username, password, statut) values(:prenom, :nom, :username, :password, :statut)");
        $req->bindValue(':prenom', $prenom, PDO::PARAM_STR);
        $req->bindValue(':nom', $nom, PDO::PARAM_STR);
        $req->bindValue(':username',$username, PDO::PARAM_STR);
        $req->bindValue(':password', hash('sha1',$password), PDO::PARAM_STR);
        $req->bindValue(':statut',$statut, PDO::PARAM_INT);

        $resultat = $req->execute();
    } catch (PDOException $e) {
        print "Erreur !: " . $e->getMessage();
        die();
    }
    return $resultat;
}

?>
