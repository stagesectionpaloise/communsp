<?php

include_once "bd.utilisateur.inc.php";

// On regarde si les entrées sont remplis, ainsi que si une session a été créée
function login($username, $password)
{
    if (!isset($_SESSION))
    {
        session_start();
    }

    $util = getUtilisateurByUtilisateur($username);
    $mdpBD = $util["password"];
    $statut = $util["statut"];
    $idMembre = $util["idMembre"];

    if ($mdpBD == hash('sha1',$password))
    {
        // le mot de passe est celui de l'utilisateur dans la base de donnees
        $_SESSION["username"] = $username;
        $_SESSION["password"] = $mdpBD;
        $_SESSION["statut"] = $statut;
        $_SESSION["idMembre"] = $idMembre;

        $token = uniqid(rand(), true);
        $_SESSION['token'] = $token;
        $_SESSION['token_time'] = time();
    }
    else
    {
        echo "Error";
    }
}

// On deconnecte l'utilisateur de la session
function logout()
{
    if (!isset($_SESSION))
    {
        session_start();
    }
    unset($_SESSION["username"]);
    unset($_SESSION["password"]);
}

// fonction pour vérifier quel utilisateur est connecté
function getUtilisateurLoggedOn()
{
    if (isLoggedOn())
    {
        $ret = $_SESSION["username"];
    }
    else
    {
        $ret = "";
    }
    return $ret;
}

// fonction pour vérifier si l'utilisateur est connecté
function isLoggedOn()
{
    if (!isset($_SESSION))
    {
        session_start();
    }
    $ret = false;

    if (isset($_SESSION["username"]))
    {
        $util = getUtilisateurByUtilisateur($_SESSION["username"]);
        if ($util["username"] == $_SESSION["username"] && $util["password"] == $_SESSION["password"])
        {
            $ret = true;
        }
    }
    return $ret;
}
?>
