<?php
//appel de la page config pour se connecter à la base de données
include_once "config.php";

//Récupération des enrainements en fonction de l'id membre
function getSeanceByIdMembre($idMembre)
{
    $resultat = array();

    try
    {
        $cnx = connexionPDO();
        $req = $cnx->prepare("select * from participer, seance, membre, creneaux where membre.idMembre=:idMembre and participer.idMembre=membre.idMembre and participer.idCreneaux=creneaux.idCreneaux and creneaux.idSeance=seance.idSeance and participer.present=1 order by dateSeance");
        $req->bindValue(':idMembre', $idMembre, PDO::PARAM_INT);

        $req->execute();

        $resultat = $req->fetchAll(PDO::FETCH_ASSOC);
    }
    catch (PDOException $e)
    {
        print "Erreur !: " . $e->getMessage();
        die();
    }
    return $resultat;
}

function getSeanceSemaineDerniereByIdMembre($idMembre, $today)
{

    $resultat = array();

    try
    {
        $cnx = connexionPDO();
        $req = $cnx->prepare("select * from participer, seance, membre, creneaux where membre.idMembre=:idMembre and participer.idMembre=membre.idMembre and participer.idCreneaux=creneaux.idCreneaux and creneaux.idSeance=seance.idSeance and participer.present=1 and datediff(:today, seance.dateSeance)<=8 and datediff(:today, seance.dateSeance)>=0");
        $req->bindValue(':idMembre', $idMembre, PDO::PARAM_INT);
        $req->bindValue(':today', $today, PDO::PARAM_STR);

        $req->execute();

        $resultat = $req->fetchAll(PDO::FETCH_ASSOC);
    }
    catch (PDOException $e)
    {
        print "Erreur !: " . $e->getMessage();
        die();
    }
    return $resultat;
}

function getSeanceMoisDernierByIdMembre($idMembre, $today)
{

    $resultat = array();

    try
    {
        $cnx = connexionPDO();
        $req = $cnx->prepare("select * from participer, seance, membre, creneaux where membre.idMembre=:idMembre and participer.idMembre=membre.idMembre and participer.idCreneaux=creneaux.idCreneaux and creneaux.idSeance=seance.idSeance and participer.present=1 and datediff(:today, seance.dateSeance)<=31 and datediff(:today, seance.dateSeance)>=0");
        $req->bindValue(':idMembre', $idMembre, PDO::PARAM_INT);
        $req->bindValue(':today', $today, PDO::PARAM_STR);

        $req->execute();

        $resultat = $req->fetchAll(PDO::FETCH_ASSOC);
    }
    catch (PDOException $e)
    {
        print "Erreur !: " . $e->getMessage();
        die();
    }
    return $resultat;
}

function getSeanceSemaineProchaineByIdMembre($idMembre, $today)
{

    $resultat = array();

    try
    {
        $cnx = connexionPDO();
        $req = $cnx->prepare("select * from participer, seance, membre, creneaux where membre.idMembre=:idMembre and participer.idMembre=membre.idMembre and participer.idCreneaux=creneaux.idCreneaux and creneaux.idSeance=seance.idSeance and participer.present=1 and datediff(seance.dateSeance, :today)<=8 and datediff(seance.dateSeance, :today)>=0");
        $req->bindValue(':idMembre', $idMembre, PDO::PARAM_INT);
        $req->bindValue(':today', $today, PDO::PARAM_STR);

        $req->execute();

        $resultat = $req->fetchAll(PDO::FETCH_ASSOC);
    }
    catch (PDOException $e)
    {
        print "Erreur !: " . $e->getMessage();
        die();
    }
    return $resultat;
}

//Récupération des enrainements pour le staff
function getSeance()
{

    $resultat = array();

    try
    {
        $cnx = connexionPDO();
        $req = $cnx->prepare("select * from seance order by dateSeance DESC");

        $req->execute();

        $resultat = $req->fetchAll(PDO::FETCH_ASSOC);
    }
    catch (PDOException $e)
    {
        print "Erreur !: " . $e->getMessage();
        die();
    }
    return $resultat;
}

function getSeanceSemaineDerniere($today)
{

    $resultat = array();

    try
    {
        $cnx = connexionPDO();
        $req = $cnx->prepare("select * from seance where datediff(:today, seance.dateSeance)<=8 and datediff(:today, seance.dateSeance)>=0");
        $req->bindValue(':today', $today, PDO::PARAM_STR);

        $req->execute();

        $resultat = $req->fetchAll(PDO::FETCH_ASSOC);
    }
    catch (PDOException $e)
    {
        print "Erreur !: " . $e->getMessage();
        die();
    }
    return $resultat;
}

function getSeanceMoisDernier($today)
{

    $resultat = array();

    try
    {
        $cnx = connexionPDO();
        $req = $cnx->prepare("select * from seance where datediff(:today, seance.dateSeance)<=31 and datediff(:today, seance.dateSeance)>=0");
        $req->bindValue(':today', $today, PDO::PARAM_STR);

        $req->execute();

        $resultat = $req->fetchAll(PDO::FETCH_ASSOC);
    }
    catch (PDOException $e)
    {
        print "Erreur !: " . $e->getMessage();
        die();
    }
    return $resultat;
}

function getSeanceSemaineProchaine($today)
{

    $resultat = array();

    try
    {
        $cnx = connexionPDO();
        $req = $cnx->prepare("select * from seance where datediff(seance.dateSeance, :today)<=8 and datediff(seance.dateSeance, :today)>=0");
        $req->bindValue(':today', $today, PDO::PARAM_STR);

        $req->execute();

        $resultat = $req->fetchAll(PDO::FETCH_ASSOC);
    }
    catch (PDOException $e)
    {
        print "Erreur !: " . $e->getMessage();
        die();
    }
    return $resultat;
}
?>
