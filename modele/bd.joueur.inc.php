<?php
//appel de la page config pour se connecter à la base de données
include_once "config.php";

//récupération dans la base de données des différents membres
function getJoueurByIdMembre($idMembre)
{

    $resultat = array();

    try
    {
        $cnx = connexionPDO();
        $req = $cnx->prepare("select * from membre where idMembre=:idMembre");
        $req->bindValue(':idMembre', $idMembre, PDO::PARAM_INT);

        $req->execute();

        $resultat = $req->fetchAll(PDO::FETCH_ASSOC);
    }
    catch (PDOException $e)
    {
        print "Erreur !: " . $e->getMessage();
        die();
    }
    return $resultat;
}

//récupération des parcours selon le membre
function getParcoursByIdMembre($idMembre)
{

    $resultat = array();

    try
    {
        $cnx = connexionPDO();
        $req = $cnx->prepare("select * from parcours where idMembre=:idMembre");
        $req->bindValue(':idMembre', $idMembre, PDO::PARAM_INT);

        $req->execute();

        $resultat = $req->fetchAll(PDO::FETCH_ASSOC);
    }
    catch (PDOException $e)
    {
        print "Erreur !: " . $e->getMessage();
        die();
    }
    return $resultat;
}

//récupération des sélections selon le membre
function getSelectionByIdMembre($idMembre)
{

    $resultat = array();

    try
    {
        $cnx = connexionPDO();
        $req = $cnx->prepare("select * from selection where idMembre=:idMembre");
        $req->bindValue(':idMembre', $idMembre, PDO::PARAM_INT);

        $req->execute();

        $resultat = $req->fetchAll(PDO::FETCH_ASSOC);
    }
    catch (PDOException $e)
    {
        print "Erreur !: " . $e->getMessage();
        die();
    }
    return $resultat;
}

//récupération des créneaux selon le membre
function getCreneauxByIdMembre($idMembre)
{

    $resultat = array();

    try
    {
        $cnx = connexionPDO();
        $req = $cnx->prepare("select creneaux.*, item.*, seance.*, participer.*, souscategorie.*, categorie.* from creneaux, item, seance, participer, souscategorie, categorie where idMembre=:idMembre and creneaux.idItem=item.idItem and seance.idSeance=creneaux.idSeance and item.idSousCategorie=souscategorie.idSousCategorie and souscategorie.idCategorie=categorie.idCategorie");
        $req->bindValue(':idMembre', $idMembre, PDO::PARAM_INT);

        $req->execute();

        $resultat = $req->fetchAll(PDO::FETCH_ASSOC);
    }
    catch (PDOException $e)
    {
        print "Erreur !: " . $e->getMessage();
        die();
    }
    return $resultat;
}

//récupération des catégorie selon le membre
function getCategorieByIdMembre($idMembre)
{

    $resultat = array();

    try
    {
        $cnx = connexionPDO();
        $req = $cnx->prepare("select distinct creneaux.*, item.*, participer.*, souscategorie.*, categorie.*, sum(duree) as sommeDuree from creneaux, item, participer, souscategorie, categorie where participer.idCreneaux=creneaux.idCreneaux and creneaux.idItem=item.idItem and item.idSousCategorie=souscategorie.idSousCategorie and souscategorie.idCategorie=categorie.idCategorie and participer.present=1 and participer.idMembre=:idMembre group by categorie.idCategorie");
        $req->bindValue(':idMembre', $idMembre, PDO::PARAM_INT);

        $req->execute();

        $resultat = $req->fetchAll(PDO::FETCH_ASSOC);
    }
    catch (PDOException $e)
    {
        print "Erreur !: " . $e->getMessage();
        die();
    }
    return $resultat;
}


//récupérer les données à partir de l'idMembre et de la categorie
function getSousCategorieByCategorieByIdMembre($idMembre, $idCategorie)
{

    $resultat = array();

    try
    {
        $cnx = connexionPDO();
        $req = $cnx->prepare("select creneaux.*, item.*, participer.*, souscategorie.*, categorie.*, sum(duree) as sommeDuree from creneaux, item, participer, souscategorie, categorie where participer.idCreneaux=creneaux.idCreneaux and creneaux.idItem=item.idItem and item.idSousCategorie=souscategorie.idSousCategorie and souscategorie.idCategorie=categorie.idCategorie and participer.present=1 and participer.idMembre=:idMembre and categorie.idCategorie=:idCategorie group by souscategorie.idSousCategorie");
        $req->bindValue(':idMembre', $idMembre, PDO::PARAM_INT);
        $req->bindValue(':idCategorie', $idCategorie, PDO::PARAM_INT);

        $req->execute();

        $resultat = $req->fetchAll(PDO::FETCH_ASSOC);
    }
    catch (PDOException $e)
    {
        print "Erreur !: " . $e->getMessage();
        die();
    }
    return $resultat;
}

//récupérer les données à partir de l'idMembre et de la categorie
function getItemBySousCategorieByIdMembre($idMembre, $idSousCategorie)
{

    $resultat = array();

    try
    {
        $cnx = connexionPDO();
        $req = $cnx->prepare("select creneaux.*, item.*, participer.*, souscategorie.*, categorie.*, sum(duree) as sommeDuree from creneaux, item, participer, souscategorie, categorie where participer.idCreneaux=creneaux.idCreneaux and creneaux.idItem=item.idItem and item.idSousCategorie=souscategorie.idSousCategorie and souscategorie.idCategorie=categorie.idCategorie and participer.present=1 and participer.idMembre=:idMembre and souscategorie.idSousCategorie=:idSousCategorie group by item.idItem");
        $req->bindValue(':idMembre', $idMembre, PDO::PARAM_INT);
        $req->bindValue(':idSousCategorie', $idSousCategorie, PDO::PARAM_INT);

        $req->execute();

        $resultat = $req->fetchAll(PDO::FETCH_ASSOC);
    }
    catch (PDOException $e)
    {
        print "Erreur !: " . $e->getMessage();
        die();
    }
    return $resultat;
}

//récupération des catégorie selon le membre et le mois
function getCategorieByIdMembreByMois($idMembre, $mois)
{

    $resultat = array();

    try
    {
        $cnx = connexionPDO();
        $req = $cnx->prepare("select creneaux.*, item.*, participer.*, souscategorie.*, categorie.*, sum(duree) as sommeDuree from creneaux, item, participer, souscategorie, categorie, seance where participer.idCreneaux=creneaux.idCreneaux and creneaux.idItem=item.idItem and item.idSousCategorie=souscategorie.idSousCategorie and souscategorie.idCategorie=categorie.idCategorie and participer.present=1 and participer.idMembre=:idMembre and MONTH(seance.dateSeance)=:mois group by categorie.idCategorie");
        $req->bindValue(':idMembre', $idMembre, PDO::PARAM_INT);
        $req->bindValue(':mois', $mois, PDO::PARAM_INT);


        $req->execute();

        $resultat = $req->fetchAll(PDO::FETCH_ASSOC);
    }
    catch (PDOException $e)
    {
        print "Erreur !: " . $e->getMessage();
        die();
    }
    return $resultat;
}


//récupérer les données à partir de l'idMembre et de la categorie et le mois
function getSousCategorieByCategorieByIdMembreByMois($idMembre, $idCategorie, $mois)
{

    $resultat = array();

    try
    {
        $cnx = connexionPDO();
        $req = $cnx->prepare("select creneaux.*, item.*, participer.*, souscategorie.*, categorie.*, sum(duree) as sommeDuree from creneaux, item, participer, souscategorie, categorie, seance where participer.idCreneaux=creneaux.idCreneaux and creneaux.idItem=item.idItem and item.idSousCategorie=souscategorie.idSousCategorie and souscategorie.idCategorie=categorie.idCategorie and participer.present=1 and participer.idMembre=:idMembre and categorie.idCategorie=:idCategorie and MONTH(seance.dateSeance)=:mois group by souscategorie.idSousCategorie");
        $req->bindValue(':idMembre', $idMembre, PDO::PARAM_INT);
        $req->bindValue(':idCategorie', $idCategorie, PDO::PARAM_INT);
        $req->bindValue(':mois', $mois, PDO::PARAM_INT);

        $req->execute();

        $resultat = $req->fetchAll(PDO::FETCH_ASSOC);
    }
    catch (PDOException $e)
    {
        print "Erreur !: " . $e->getMessage();
        die();
    }
    return $resultat;
}

//récupérer les données à partir de l'idMembre et de la categorie et le mois
function getItemBySousCategorieByIdMembreByMois($idMembre, $idSousCategorie, $mois)
{

    $resultat = array();

    try
    {
        $cnx = connexionPDO();
        $req = $cnx->prepare("select creneaux.*, item.*, participer.*, souscategorie.*, categorie.*, sum(duree) as sommeDuree from creneaux, item, participer, souscategorie, categorie, seance where participer.idCreneaux=creneaux.idCreneaux and creneaux.idItem=item.idItem and item.idSousCategorie=souscategorie.idSousCategorie and souscategorie.idCategorie=categorie.idCategorie and participer.present=1 and participer.idMembre=:idMembre and souscategorie.idSousCategorie=:idSousCategorie and MONTH(seance.dateSeance)=:mois group by item.idItem");
        $req->bindValue(':idMembre', $idMembre, PDO::PARAM_INT);
        $req->bindValue(':idSousCategorie', $idSousCategorie, PDO::PARAM_INT);
        $req->bindValue(':mois', $mois, PDO::PARAM_INT);

        $req->execute();

        $resultat = $req->fetchAll(PDO::FETCH_ASSOC);
    }
    catch (PDOException $e)
    {
        print "Erreur !: " . $e->getMessage();
        die();
    }
    return $resultat;
}

//récupération des joueurs selon leurs situations
function getListeJoueur($idSituation)
{

    $resultat = array();

    try
    {
        $cnx = connexionPDO();
        $req = $cnx->prepare("select * from membre where statut=2 and idSituation=:idSituation");
        $req->bindValue(':idSituation', $idSituation, PDO::PARAM_INT);

        $req->execute();

        $resultat = $req->fetchAll(PDO::FETCH_ASSOC);
    }
    catch (PDOException $e)
    {
        print "Erreur !: " . $e->getMessage();
        die();
    }
    return $resultat;
}

function getNombreJoueurBySituation($idSituation)
{

    $resultat = array();

    try
    {
        $cnx = connexionPDO();
        $req = $cnx->prepare("select count(*) as nbJoueur from membre where statut=2 and idSituation=:idSituation");
        $req->bindValue(':idSituation', $idSituation, PDO::PARAM_INT);

        $req->execute();

        $resultat = $req->fetchAll(PDO::FETCH_ASSOC);
    }
    catch (PDOException $e)
    {
        print "Erreur !: " . $e->getMessage();
        die();
    }
    return $resultat;
}

//mise à jour des situations
function updateSituationJoueur($idSituation, $idMembre)
{

    try
    {
        $cnx = connexionPDO();
        $req = $cnx->prepare("update membre set idSituation = :idSituation where idMembre = :idMembre");
        $req->bindValue(':idSituation', $idSituation, PDO::PARAM_INT);
        $req->bindValue(':idMembre', $idMembre, PDO::PARAM_INT);

        $req->execute();

    }
    catch (PDOException $e)
    {
        print "Erreur !: " . $e->getMessage();
        die();
    }

}

//récupération des poste selon le membre
function getPosteByIdMembre($idMembre)
{

    $resultat = array();

    try
    {
        $cnx = connexionPDO();
        $req = $cnx->prepare("select * from membre, poste where membre.idPoste = poste.idPoste and idMembre=:idMembre");
        $req->bindValue(':idMembre', $idMembre, PDO::PARAM_INT);

        $req->execute();

        $resultat = $req->fetchAll(PDO::FETCH_ASSOC);
    }
    catch (PDOException $e)
    {
        print "Erreur !: " . $e->getMessage();
        die();
    }
    return $resultat;
}

function getPoste2ByIdMembre($idMembre)
{

    $resultat = array();

    try
    {
        $cnx = connexionPDO();
        $req = $cnx->prepare("select * from membre, poste where membre.idPoste2 = poste.idPoste and idMembre=:idMembre");
        $req->bindValue(':idMembre', $idMembre, PDO::PARAM_INT);

        $req->execute();

        $resultat = $req->fetchAll(PDO::FETCH_ASSOC);
    }
    catch (PDOException $e)
    {
        print "Erreur !: " . $e->getMessage();
        die();
    }
    return $resultat;
}

//mise à jour des infos joueur
function updateInfoJoueur($prenom, $nom ,$username, $mail, $tel, $dateNaissance, $pays, $poste, $poste2, $idMembre, $lienPhoto, $dureeContrat, $scolaire, $JIFF)
{

    try
    {
      $cnx = connexionPDO();

      if($prenom!="")
      {
        $req = $cnx->prepare("update membre set prenom = :prenom where idMembre = :idMembre");
        $req->bindValue(':prenom', $prenom, PDO::PARAM_STR);
        $req->bindValue(':idMembre', $idMembre, PDO::PARAM_INT);
        $req->execute();
      }

      if($nom!="")
      {
        $req = $cnx->prepare("update membre set nom = :nom where idMembre = :idMembre");
        $req->bindValue(':nom', $nom, PDO::PARAM_STR);
        $req->bindValue(':idMembre', $idMembre, PDO::PARAM_INT);
        $req->execute();
      }

      if($username!="")
      {
        $req = $cnx->prepare("update membre set username = :username where idMembre = :idMembre");
        $req->bindValue(':username', $username, PDO::PARAM_STR);
        $req->bindValue(':idMembre', $idMembre, PDO::PARAM_INT);
        $req->execute();
      }

      if($mail!="")
      {
        $req = $cnx->prepare("update membre set mail = :mail where idMembre = :idMembre");
        $req->bindValue(':mail', $mail, PDO::PARAM_STR);
        $req->bindValue(':idMembre', $idMembre, PDO::PARAM_INT);
        $req->execute();
      }

      if($tel!="")
      {
        $req = $cnx->prepare("update membre set tel = :tel where idMembre = :idMembre");
        $req->bindValue(':tel', $tel, PDO::PARAM_INT);
        $req->bindValue(':idMembre', $idMembre, PDO::PARAM_INT);
        $req->execute();
      }

      if($dateNaissance!="")
      {
        $req = $cnx->prepare("update membre set dateNaissance = :dateNaissance where idMembre = :idMembre");
        $req->bindValue(':dateNaissance', $dateNaissance, PDO::PARAM_STR);
        $req->bindValue(':idMembre', $idMembre, PDO::PARAM_INT);
        $req->execute();
      }

      if($pays!="")
      {
        $req = $cnx->prepare("update membre set pays = :pays where idMembre = :idMembre");
        $req->bindValue(':pays', $pays, PDO::PARAM_STR);
        $req->bindValue(':idMembre', $idMembre, PDO::PARAM_INT);
        $req->execute();
      }

      if($poste!=0)
      {
        $req = $cnx->prepare("update membre set idPoste = :idPoste where idMembre = :idMembre");
        $req->bindValue(':idPoste', $poste, PDO::PARAM_INT);
        $req->bindValue(':idMembre', $idMembre, PDO::PARAM_INT);
        $req->execute();
      }

      if($poste2!=0)
      {
        $req = $cnx->prepare("update membre set idPoste2 = :idPoste2 where idMembre = :idMembre");
        $req->bindValue(':idPoste2', $poste2, PDO::PARAM_INT);
        $req->bindValue(':idMembre', $idMembre, PDO::PARAM_INT);
        $req->execute();
      }

      if($lienPhoto!=NULL)
      {
        $req = $cnx->prepare("update membre set lienPhoto = :lienPhoto where idMembre = :idMembre");
        $req->bindValue(':lienPhoto', $lienPhoto, PDO::PARAM_STR);
        $req->bindValue(':idMembre', $idMembre, PDO::PARAM_INT);
        $req->execute();
      }

      if($dureeContrat!="")
      {
        $req = $cnx->prepare("update membre set dureeContrat = :dureeContrat where idMembre = :idMembre");
        $req->bindValue(':dureeContrat', $dureeContrat, PDO::PARAM_INT);
        $req->bindValue(':idMembre', $idMembre, PDO::PARAM_INT);
        $req->execute();
      }

      if($scolaire!="")
      {
        $req = $cnx->prepare("update membre set scolaire = :scolaire where idMembre = :idMembre");
        $req->bindValue(':scolaire', $scolaire, PDO::PARAM_STR);
        $req->bindValue(':idMembre', $idMembre, PDO::PARAM_INT);
        $req->execute();
      }

      if($JIFF!=0)
      {
        $req = $cnx->prepare("update membre set JIFF = :JIFF where idMembre = :idMembre");
        $req->bindValue(':JIFF', $JIFF, PDO::PARAM_INT);
        $req->bindValue(':idMembre', $idMembre, PDO::PARAM_INT);
        $req->execute();
      }

    }
    catch (PDOException $e)
    {
        print "Erreur !: " . $e->getMessage();
        die();
    }
}

function getCategorie()
{

    $resultat = array();

    try
    {
        $cnx = connexionPDO();
        $req = $cnx->prepare("select * from categorie");

        $req->execute();

        $resultat = $req->fetchAll(PDO::FETCH_ASSOC);
    }
    catch (PDOException $e)
    {
        print "Erreur !: " . $e->getMessage();
        die();
    }
    return $resultat;
}

function getSousCategorieByIdCategorie($idCategorie)
{
  $resultat = array();

  try
  {
      $cnx = connexionPDO();
      $req = $cnx->prepare("select * from souscategorie where souscategorie.idCategorie = :idCategorie");
      $req->bindValue(':idCategorie', $idCategorie, PDO::PARAM_INT);

      $req->execute();

      $resultat = $req->fetchAll(PDO::FETCH_ASSOC);
  }
  catch (PDOException $e)
  {
      print "Erreur !: " . $e->getMessage();
      die();
  }
  return $resultat;
}

function addParcours($idMembre, $libParcours)
{
  try
  {
      $cnx = connexionPDO();
      $req = $cnx->prepare("insert into parcours (idMembre, libParcours) values(:idMembre, :libParcours)");
      $req->bindValue(':libParcours', $libParcours, PDO::PARAM_STR);
      $req->bindValue(':idMembre', $idMembre, PDO::PARAM_INT);


      $resultat = $req->execute();
  }
  catch (PDOException $e)
  {
      print "Erreur !: " . $e->getMessage();
      die();
  }
  return $resultat;
}

function supprParcours($idParcours)
{
  try
  {
      $cnx = connexionPDO();
      $req = $cnx->prepare("delete from parcours where idParcours = :idParcours");
      $req->bindValue(':idParcours', $idParcours, PDO::PARAM_INT);

      $resultat = $req->execute();
  }
  catch (PDOException $e)
  {
      print "Erreur !: " . $e->getMessage();
      die();
  }
  return $resultat;
}

function addSelection($idMembre, $libSelection)
{
  try
  {
      $cnx = connexionPDO();
      $req = $cnx->prepare("insert into selection (idMembre, libSelection) values(:idMembre, :libSelection)");
      $req->bindValue(':libSelection', $libSelection, PDO::PARAM_STR);
      $req->bindValue(':idMembre', $idMembre, PDO::PARAM_INT);

      $resultat = $req->execute();
  }
  catch (PDOException $e)
  {
      print "Erreur !: " . $e->getMessage();
      die();
  }
  return $resultat;
}

function supprSelection($idSelection)
{
  try
  {
      $cnx = connexionPDO();
      $req = $cnx->prepare("delete from selection where idSelection = :idSelection");
      $req->bindValue(':idSelection', $idSelection, PDO::PARAM_INT);

      $resultat = $req->execute();
  }
  catch (PDOException $e)
  {
      print "Erreur !: " . $e->getMessage();
      die();
  }
  return $resultat;
}

function updateInfoCompte($prenom, $nom ,$username, $mail, $tel, $dateNaissance, $idMembre)
{

    try
    {
      $cnx = connexionPDO();

      if($prenom!="")
      {
        $req = $cnx->prepare("update membre set prenom = :prenom where idMembre = :idMembre");
        $req->bindValue(':prenom', $prenom, PDO::PARAM_STR);
        $req->bindValue(':idMembre', $idMembre, PDO::PARAM_INT);
        $req->execute();
      }

      if($nom!="")
      {
        $req = $cnx->prepare("update membre set nom = :nom where idMembre = :idMembre");
        $req->bindValue(':nom', $nom, PDO::PARAM_STR);
        $req->bindValue(':idMembre', $idMembre, PDO::PARAM_INT);
        $req->execute();
      }

      if($username!="")
      {
        $req = $cnx->prepare("update membre set username = :username where idMembre = :idMembre");
        $req->bindValue(':username', $username, PDO::PARAM_STR);
        $req->bindValue(':idMembre', $idMembre, PDO::PARAM_INT);
        $req->execute();
      }

      if($mail!="")
      {
        $req = $cnx->prepare("update membre set mail = :mail where idMembre = :idMembre");
        $req->bindValue(':mail', $mail, PDO::PARAM_STR);
        $req->bindValue(':idMembre', $idMembre, PDO::PARAM_INT);
        $req->execute();
      }

      if($tel!="")
      {
        $req = $cnx->prepare("update membre set tel = :tel where idMembre = :idMembre");
        $req->bindValue(':tel', $tel, PDO::PARAM_INT);
        $req->bindValue(':idMembre', $idMembre, PDO::PARAM_INT);
        $req->execute();
      }

      if($dateNaissance!="")
      {
        $req = $cnx->prepare("update membre set dateNaissance = :dateNaissance where idMembre = :idMembre");
        $req->bindValue(':dateNaissance', $dateNaissance, PDO::PARAM_STR);
        $req->bindValue(':idMembre', $idMembre, PDO::PARAM_INT);
        $req->execute();
      }
    }
    catch (PDOException $e)
    {
        print "Erreur !: " . $e->getMessage();
        die();
    }
}

function updateMDP($password, $idMembre)
{
    try
    {
      $cnx = connexionPDO();

      $req = $cnx->prepare("update membre set password = :password where idMembre = :idMembre");
      $req->bindValue(':password', $password, PDO::PARAM_STR);
      $req->bindValue(':idMembre', $idMembre, PDO::PARAM_INT);
      $req->execute();
    }
    catch (PDOException $e)
    {
        print "Erreur !: " . $e->getMessage();
        die();
    }
}

function getListeMembreByStatut($statut)
{
  $resultat = array();

  try
  {
      $cnx = connexionPDO();
      $req = $cnx->prepare("select * from membre where statut = :statut");
      $req->bindValue(':statut', $statut, PDO::PARAM_INT);

      $req->execute();

      $resultat = $req->fetchAll(PDO::FETCH_ASSOC);
  }

  catch (PDOException $e)
  {
      print "Erreur !: " . $e->getMessage();
      die();
  }

  return $resultat;
}

function supprMembre($idMembre)
{
  try
  {
      $cnx = connexionPDO();

      $req = $cnx->prepare("delete from participer where idMembre = :idMembre");
      $req->bindValue(':idMembre', $idMembre, PDO::PARAM_INT);
      $resultat = $req->execute();

      $req = $cnx->prepare("delete from parcours where idMembre = :idMembre");
      $req->bindValue(':idMembre', $idMembre, PDO::PARAM_INT);
      $resultat = $req->execute();

      $req = $cnx->prepare("delete from selection where idMembre = :idMembre");
      $req->bindValue(':idMembre', $idMembre, PDO::PARAM_INT);
      $resultat = $req->execute();

      $req = $cnx->prepare("delete from morpho where idMembre = :idMembre");
      $req->bindValue(':idMembre', $idMembre, PDO::PARAM_INT);
      $resultat = $req->execute();

      $req = $cnx->prepare("delete from membre where idMembre = :idMembre");
      $req->bindValue(':idMembre', $idMembre, PDO::PARAM_INT);
      $resultat = $req->execute();
  }
  catch (PDOException $e)
  {
      print "Erreur !: " . $e->getMessage();
      die();
  }
  return $resultat;
}
?>
