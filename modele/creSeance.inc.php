<?php
include_once "config.php";

// Fonction pour récupérer toutes les dates de la table entrainement
function getDateSeance()
{
  $resultat = array();

  try
  {
    $cnx = connexionPDO();
    $req = $cnx->prepare("select dateSeance from seance");
    $req->execute();

    $ligne = $req->fetch(PDO::FETCH_ASSOC);
    while ($ligne)
    {
      $resultat[] = $ligne;
      $ligne = $req->fetch(PDO::FETCH_ASSOC);
    }
  }
  catch (PDOException $e) {
    print "Erreur !: " . $e->getMessage();
    die();
  }
  return $resultat;
}


function getSeanceByDateSeance($DateSeance)
{
  try {
    $cnx = connexionPDO();
    $req = $cnx->prepare("select idSeance from seance where dateSeance=:DateSeance");
    $req->bindValue(':DateSeance', $DateSeance, PDO::PARAM_STR);
    $req->execute();
    $resultat = $req->fetch(PDO::FETCH_ASSOC);
  }
  catch (PDOExcepetion $e) {
    print "Erreur !: " . $e->getMessage();
    die();
  }
  return $resultat;
}

// Fonction pour ajouter un creneau sur l'entrainement de la journée
function addSeance($DateSeance, $heureDebut, $heureFin)
{
  try
  {
    $cnx = connexionPDO();
    $req = $cnx->prepare("insert into seance (dateSeance, heureDebut, heureFin) values(:dateSeance, :heureDebut, :heureFin)");
    $req->bindValue(':dateSeance', $DateSeance, PDO::PARAM_STR);
    $req->bindValue(':heureDebut', $heureDebut, PDO::PARAM_STR);
    $req->bindValue(':heureFin', $heureFin, PDO::PARAM_STR);

    $resultat = $req->execute();
  }
  catch (PDOException $e)
  {
    print "Erreur !: " . $e->getMessage();
    die();
  }
  return $resultat;
}

function addParticiper($idMembre, $idCreneaux)
{
  try
  {
    $cnx = connexionPDO();
    $req = $cnx->prepare("insert into participer (idCreneaux, idMembre) values(:idCreneaux, :idMembre)");
    $req->bindValue(':idCreneaux', $idCreneaux, PDO::PARAM_INT);
    $req->bindValue(':idMembre', $idMembre, PDO::PARAM_INT);

    $resultat = $req->execute();
  }
  catch (PDOException $e)
  {
    print "Erreur !: " . $e->getMessage();
    die();
  }
  return $resultat;
}

function getEntraineurByStatus()
{
  try
  {
    $cnx = connexionPDO();
    $req = $cnx->prepare("select * from membre where statut=1");
    $req->execute();
    $resultat=$req->fetchAll(PDO::FETCH_ASSOC);
  }
  catch (PDOException $e)
  {
    print "Erreur !: " . $e->getMessage();
    die();
  }
  return $resultat;
}

function getItembyidSousCategorie($idSousCategorie)
{
  try {
    $cnx = connexionPDO();
    $req = $cnx->prepare("select * from item as i,souscategorie as s where i.idSousCategorie = s.idSousCategorie and s.idSousCategorie=:idSousCategorie");
    $req->bindValue(':idSousCategorie', $idSousCategorie, PDO::PARAM_INT);
    $req->execute();
    $resultat=$req->fetchAll(PDO::FETCH_ASSOC);
  }
  catch (PDOException $e) {
    print "Erreur !: " ; $e->getMessage();
    die();
  }
  return $resultat;
}

function getCreneau($idEntr)
{
  try
  {
    $cnx = connexionPDO();
    $req = $cnx->prepare("select * from creneaux,item where creneaux.idItem = item.idItem and idEntr=:idEntr");
    $req->bindValue(':idEntr', $idEntr, PDO::PARAM_INT);
    $req->execute();
    $resultat=$req->fetchAll(PDO::FETCH_ASSOC);
  }
  catch (PDOException $e)
  {
    print "Erreur !: " . $e->getMessage();
    die();
  }
  return $resultat;
}

function getTousCreneaux()
{
  try
  {
    $cnx = connexionPDO();
    $req = $cnx->prepare("select * from creneaux");
    $req->execute();
    $resultat=$req->fetchAll(PDO::FETCH_ASSOC);
  }
  catch (PDOException $e)
  {
    print "Erreur !: " . $e->getMessage();
    die();
  }
  return $resultat;
}

function getIdEntrByIdCreneau($idCreneaux)
{
  try
  {
    $cnx = connexionPDO();
    $req = $cnx->prepare("select * from creneaux, faire, membre where creneaux.idCreneaux=faire.idCreneaux and faire.idMembre=membre.idMembre and creneaux.idCreneaux=:idCreneaux");
    $req->bindValue(':idCreneaux', $idCreneaux, PDO::PARAM_INT);
    $req->execute();
    $resultat=$req->fetchAll(PDO::FETCH_ASSOC);
  }
  catch (PDOException $e)
  {
    print "Erreur !: " . $e->getMessage();
    die();
  }
  return $resultat;
}

function getCategorie()
{
  try
  {
    $cnx = connexionPDO();
    $req = $cnx->prepare("select * from categorie");
    $req->execute();
    $resultat=$req->fetchAll(PDO::FETCH_ASSOC);
  }
  catch (PDOException $e)
  {
    print "Erreur !: " . $e->getMessage();
    die();
  }
  return $resultat;
}

function getSouscatégoriebyCategorie($idCategorie)
{
  try {
    $cnx = connexionPDO();
    $req = $cnx->prepare("select * from souscategorie, categorie where souscategorie.idCategorie = categorie.idCategorie and souscategorie.idCategorie = :idCategorie");
    $req->bindValue(':idCategorie', $idCategorie, PDO::PARAM_INT);
    $req->execute();
    $resultat=$req->fetchAll(PDO::FETCH_ASSOC);
  }
  catch (PDOException $e)
   {
    print "Erreur !: " . $e->getMessage();
    die();
  }
  return $resultat;
}

function getSouscatégoriebyidCategorie($idCategorie)
{
  try
  {
    $cnx = connexionPDO();
    $req = $cnx->prepare("select * from souscategorie as where s.idCategorie=idCategorie");
    $req->bindValue(':idCategorie', $idCategorie, PDO::PARAM_INT);
    $req->execute();
    $resultat=$req->fetchAll(PDO::FETCH_ASSOC);
  }
  catch (PDOException $e)
  {
    print "Erreur !: " . $e->getMessage();
    die();
  }
  return $resultat;
}

function addCreneau($idSeance, $duree)
{
  try {
   $cnx = connexionPDO();
   $req = $cnx->prepare("insert into creneaux (idSeance, duree) values (:idSeance, :duree)");
   $req->bindValue(':idSeance', $idSeance, PDO::PARAM_INT);
   $req->bindValue(':duree', $duree, PDO::PARAM_INT);
   $resultat = $req->execute();
  }
  catch (PDOException $e)
  {
    print "Erreur !: " . $e->getMessage();
    die();
  }
  return $resultat;
}

function getMembresbyStatut()
{
  try {
    $cnx = connexionPDO();
    $req = $cnx->prepare("select * from membre where statut = 2");
    $req->execute();
    $resultat=$req->fetchAll(PDO::FETCH_ASSOC);
  }
  catch (PDOException $e)
  {
    print "Erreur !: " . $e->getMessage();
    die();
  }
  return $resultat;
}

function addItembyidSeance($idCreneau,$idSeance,$idItem)
{
  try {
    $cnx = connexionPDO();
    $req = $cnx->prepare("update creneaux SET idItem = :idItem where idCreneaux = :idCreneaux and idSeance = :idSeance");
    $req->bindValue(":idSeance",$idSeance, PDO::PARAM_INT);
    $req->bindValue(":idCreneaux",$idCreneau, PDO::PARAM_INT);
    $req->bindValue(":idItem", $idItem, PDO::PARAM_INT);
    $resultat = $req->execute();
  }
  catch (PDOException $e)
  {
    print "Erreur ! :" . $e->getMessage();
    die();
  }
  return $resultat;
}

function getTousSeance()
{
  try {
    $cnx = connexionPDO();
    $req = $cnx->prepare("select * from seance");
    $req->execute();
    $resultat=$req->fetchAll(PDO::FETCH_ASSOC);
  }
  catch (PDOException $e)
  {
    print "Erreur ! :" . $e->getMessage();
    die();
  }
  return $resultat;
}

function getTousCreneauxbyIdSeance($idSeance)
{
  try
  {
    $cnx = connexionPDO();
    $req = $cnx->prepare("select * from creneaux where creneaux.idSeance = :idSeance");
    $req->bindValue(":idSeance",$idSeance, PDO::PARAM_INT);
    $req->execute();
    $resultat=$req->fetchAll(PDO::FETCH_ASSOC);
  }
  catch (PDOException $e)
  {
    print "Erreur !: " . $e->getMessage();
    die();
  }
  return $resultat;
}

function getSousCategbyidItem($idItem)
{
  try
  {
    $cnx = connexionPDO();
    $req = $cnx->prepare("select * from item where item.idItem = :idItem");
    $req->bindValue(":idItem", $idItem, PDO::PARAM_INT);
    $req->execute();
    $resultat=$req->fetch(PDO::FETCH_ASSOC);
  }
  catch (PDOException $e)
  {
    print "Erreur !: " . $e->getMessage();
    die();
  }
  return $resultat;
}

function getLibelSousCategByIdSousCateg($idSousCategorie)
{
  try
  {
    $cnx = connexionPDO();
    $req = $cnx->prepare("select * from souscategorie where idSousCategorie = :idSousCategorie");
    $req->bindValue(":idSousCategorie", $idSousCategorie, PDO::PARAM_INT);
    $req->execute();
    $resultat=$req->fetch(PDO::FETCH_ASSOC);
  }
  catch (PDOException $e)
  {
    print "Erreur !: " . $e->getMessage();
    die();
  }
  return $resultat;
}

function getLibelCategoriebyIdSousCateg($idCategorie)
{
  try
  {
    $cnx = connexionPDO();
    $req = $cnx->prepare("select * from categorie where idCategorie = :idCategorie");
    $req->bindValue(":idCategorie", $idCategorie, PDO::PARAM_INT);
    $req->execute();
    $resultat=$req->fetch(PDO::FETCH_ASSOC);
  }

  catch (PDOException $e)
  {
    print "Erreur !: " . $e->getMessage();
    die();
  }
  return $resultat;
}

//listing joueur par créneaux
//puis comparaison avec $joueurpresent pour savoir si present égale à 1 ou 0
function modificationPresent($idMembre, $idCreneaux, $present)
{
  try
  {
      $cnx = connexionPDO();
      if ($present==1)
      {
        $req = $cnx->prepare("update participer set present = 1 where idMembre = :idMembre and idCreneaux=:idCreneaux");
        $req->bindValue(':idCreneaux', $idCreneaux, PDO::PARAM_INT);
        $req->bindValue(':idMembre', $idMembre, PDO::PARAM_INT);
      }
      if ($present==0)
      {
        $req = $cnx->prepare("update participer set present = 0 where idMembre = :idMembre and idCreneaux=:idCreneaux");
        $req->bindValue(':idCreneaux', $idCreneaux, PDO::PARAM_INT);
        $req->bindValue(':idMembre', $idMembre, PDO::PARAM_INT);
      }
      $req->execute();
  }
  catch (PDOException $e)
  {
      print "Erreur !: " . $e->getMessage();
      die();
  }
}

function supprimerCreneau($idCreneau)
{
  try
  {
    $cnx = connexionPDO();
    $req = $cnx->prepare("delete from participer where idCreneaux = :idCreneau");
    $req->bindValue(':idCreneau', $idCreneau, PDO::PARAM_INT);
    $req->execute();

    $req = $cnx->prepare("delete from creneaux where idCreneaux = :idCreneau");
    $req->bindValue(':idCreneau', $idCreneau, PDO::PARAM_INT);
    $req->execute();
  }
  catch(PDOException $e)
  {
    print "Erreur !: " . $e->getMessage();
    die();
  }
}

function getidMembreByidCreneau($idCreneau)
{
  try
  {
    $cnx = connexionPDO();
    $req = $cnx->prepare("select * from participer where idCreneaux=:idCreneau");
    $req->bindValue(':idCreneau', $idCreneau, PDO::PARAM_INT);
    $req->execute();
    $resultat=$req->fetchAll(PDO::FETCH_ASSOC);
  }
  catch(PDOException $e)
  {
    print "Erreur !: " . $e->getMessage();
    die();
  }
  return $resultat;
}


function getMembrebystatus()
{
  try
  {
    $cnx = connexionPDO();
    $req = $cnx->prepare("select * from membre where statut = 2");
    $req->execute();
    $resultat=$req->fetchAll(PDO::FETCH_ASSOC);
  }
  catch(PDOException $e)
  {
    print "Erreur !: " . $e->getMessage();
    die();
  }
  return $resultat;
}


function getMembresbyPresent($idCreneau, $statut)
{
  try
  {
    $cnx = connexionPDO();
    $req = $cnx->prepare("select membre.*,  participer.* from participer, membre where membre.idMembre=participer.idMembre and present=1 and idCreneaux=:idCreneau and statut=:statut");
    $req->bindValue(':idCreneau', $idCreneau, PDO::PARAM_INT);
    $req->bindValue(':statut', $statut, PDO::PARAM_INT);
    $req->execute();
    $resultat=$req->fetchAll(PDO::FETCH_ASSOC);
  }
  catch(PDOException $e)
  {
    print "Erreur !: " . $e->getMessage();
    die ();
  }
  return $resultat;
}


function getMembresbyidMembre($idMembre)
{
  try
  {
    $cnx = connexionPDO();
    $req = $cnx->prepare("select * from membre where idMembre=:idMembre");
    $req->bindValue(':idMembre', $idMembre, PDO::PARAM_INT);
    $req->execute();
    $resultat=$req->fetchAll(PDO::FETCH_ASSOC);
  }
  catch(PDOException $e)
  {
    print "Erreur !: " . $e->getMessage();
    die();
  }
  return $resultat;
}


function updateCreneau($idCreneau, $duree)
{
  try
  {
    $cnx = connexionPDO();
    $req = $cnx->prepare("update creneaux set duree=:duree where idCreneaux=:idCreneau");
    $req->bindValue(':duree', $duree, PDO::PARAM_INT);
    $req->bindValue(':idCreneau', $idCreneau, PDO::PARAM_INT);
    $req->execute();
  }
  catch (PDOException $e)
  {
    print "Erreur !: " . $e->getMessage();
    die();
  }
}


function getMembresbyAbsent($idCreneau, $statut)
{
  try
  {
    $cnx = connexionPDO();
    $req = $cnx->prepare("select membre.*,  participer.* from participer, membre where membre.idMembre=participer.idMembre and present=0 and idCreneaux=:idCreneau and statut=:statut");
    $req->bindValue(':idCreneau', $idCreneau, PDO::PARAM_INT);
    $req->bindValue(':statut', $statut, PDO::PARAM_INT);
    $req->execute();
    $resultat=$req->fetchAll(PDO::FETCH_ASSOC);
  }
  catch(PDOException $e)
  {
    print "Erreur !: " . $e->getMessage();
    die ();
  }
  return $resultat;
}


?>
