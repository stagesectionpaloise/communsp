<?php
//appel de la page config pour se connecter à la base de données
include_once "config.php";

function supprItem($idItem)
{
  try
  {
      $cnx = connexionPDO();

      $req = $cnx->prepare("delete from participer, creneaux where participer.idCreneaux = creneaux.idCreneaux and creneaux.idItem = :idItem");
      $req->bindValue(':idItem', $idItem, PDO::PARAM_INT);
      $resultat = $req->execute();

      $req = $cnx->prepare("delete from item where idItem = :idItem");
      $req->bindValue(':idItem', $idItem, PDO::PARAM_INT);
      $resultat = $req->execute();
  }
  catch (PDOException $e)
  {
      print "Erreur !: " . $e->getMessage();
      die();
  }
  return $resultat;
}

function supprSousCategorie($idSousCategorie)
{
  try
  {
      $cnx = connexionPDO();

      $req = $cnx->prepare("delete from participer, creneaux, souscategorie where souscategorie.idSousCategorie = item.idSousCategorie and participer.idCreneaux = creneaux.idCreneaux and souscategorie.idSousCategorie = :idSousCategorie");
      $req->bindValue(':idSousCategorie', $idSousCategorie, PDO::PARAM_INT);
      $resultat = $req->execute();

      $req = $cnx->prepare("delete from item where idSousCategorie = :idSousCategorie");
      $req->bindValue(':idSousCategorie', $idSousCategorie, PDO::PARAM_INT);
      $resultat = $req->execute();

      $req = $cnx->prepare("delete from participer, creneaux where participer.idCreneaux = creneaux.idCreneaux and creneaux.idItem = :idItem");
      $req->bindValue(':idSousCategorie', $idSousCategorie, PDO::PARAM_INT);
      $resultat = $req->execute();
  }
  catch (PDOException $e)
  {
      print "Erreur !: " . $e->getMessage();
      die();
  }
  return $resultat;
}

function supprCategorie($idCategorie)
{
  try
  {
      $cnx = connexionPDO();

      $req = $cnx->prepare("delete from participer, creneaux, souscategorie, categorie where categorie.idCategorie = souscategorie.idCategorie and souscategorie.idSousCategorie = item.idSousCategorie and participer.idCreneaux = creneaux.idCreneaux and categorie.idCategorie = :idCategorie");
      $req->bindValue(':idCategorie', $idCategorie, PDO::PARAM_INT);
      $resultat = $req->execute();

      $req = $cnx->prepare("delete from item where idSousCategorie = :idSousCategorie");
      $req->bindValue(':idCategorie', $idCategorie, PDO::PARAM_INT);
      $resultat = $req->execute();

      $req = $cnx->prepare("delete from participer, creneaux where participer.idCreneaux = creneaux.idCreneaux and creneaux.idItem = :idItem");
      $req->bindValue(':idCategorie', $idCategorie, PDO::PARAM_INT);
      $resultat = $req->execute();
  }
  catch (PDOException $e)
  {
      print "Erreur !: " . $e->getMessage();
      die();
  }
  return $resultat;
}

function addCategorie($libCategorie)
{
  try
  {
      $cnx = connexionPDO();
      $req = $cnx->prepare("insert into categorie (libCategorie) values(:libCategorie)");
      $req->bindValue(':libCategorie', $libCategorie, PDO::PARAM_STR);

      $resultat = $req->execute();
  }
  catch (PDOException $e)
  {
      print "Erreur !: " . $e->getMessage();
      die();
  }
  return $resultat;
}

function addSousCategorie($libSousCategorie, $idCategorie)
{
  try
  {
      $cnx = connexionPDO();
      $req = $cnx->prepare("insert into souscategorie (libSousCategorie, idCategorie) values(:libSousCategorie, :idCategorie)");
      $req->bindValue(':libSousCategorie', $libSousCategorie, PDO::PARAM_STR);
      $req->bindValue(':idCategorie', $idCategorie, PDO::PARAM_INT);

      $resultat = $req->execute();
  }
  catch (PDOException $e)
  {
      print "Erreur !: " . $e->getMessage();
      die();
  }
  return $resultat;
}

function addItem($libItem, $idSousCategorie)
{
  try
  {
      $cnx = connexionPDO();
      $req = $cnx->prepare("insert into item (libItem, idSousCategorie) values(:libItem, :idSousCategorie)");
      $req->bindValue(':libItem', $libItem, PDO::PARAM_STR);
      $req->bindValue(':idSousCategorie', $idSousCategorie, PDO::PARAM_INT);

      $resultat = $req->execute();
  }
  catch (PDOException $e)
  {
      print "Erreur !: " . $e->getMessage();
      die();
  }
  return $resultat;
}

?>
